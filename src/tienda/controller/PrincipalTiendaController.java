/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tienda.controller;

import java.net.URL;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import tienda.modelo.*;

/**
 * FXML Controller class
 *
 * @author jhone
 */
public class PrincipalTiendaController extends Application implements Initializable {
    
    private Tienda tienda;

    public static void main(String[] args) {
        launch(args);
    }
    @FXML
    private Button limpiarButton;
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/tienda/vista/PrincipalTienda.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle("TIENDA");
        stage.show();
    }
    
    @FXML
    private ListView<Producto> listView;
    @FXML
    private TitledPane agregarProductoTitle;
    @FXML
    private ComboBox<String> tipo_Producto;
    @FXML
    private TextField nombre_Producto;
    @FXML
    private TextField cant_Producto;
    @FXML
    private TextField precio_Producto;
    @FXML
    private Button aceptarButton;
    @FXML
    private Button cancelarButton;
    @FXML
    private Button hacerPedidoButton;
    @FXML
    private Button venderProductoButton;
    @FXML
    private TextField cant_VenderProducto;
    @FXML
    private TextField cant_HacerPedido;
    @FXML
    private Button mostrarEstadisticasButton;
    @FXML
    private TextArea textArea;
    @FXML
    private TitledPane categoriasTitle;
    @FXML
    private Button cat_PapeleriaButton;
    @FXML
    private Button cat_SupermercadoButton;
    @FXML
    private Button cat_DrogueriaButton;
    
    private ObservableList<Producto> productos;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.tienda = new Tienda();
        this.listView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        productos = FXCollections.observableArrayList();
        this.listView.setItems(productos);
        ObservableList<String> aux = FXCollections.observableArrayList("PAPELERIA","SUPERMERCADO","DROGUERIA");
        this.tipo_Producto.setItems(aux);
    }  
    
    public void crearDialogo(String title,String context){
        Alert dialogo;
        dialogo = new Alert(Alert.AlertType.INFORMATION);
        dialogo.setTitle(title);
        dialogo.setContentText(context);
        dialogo.initStyle(StageStyle.UTILITY);
        java.awt.Toolkit.getDefaultToolkit().beep();
        dialogo.showAndWait();
    }

    @FXML
    private void handleAceptarAction(ActionEvent event) {
        try{
            String tipo = this.tipo_Producto.getValue();
            String nombre = this.nombre_Producto.getText();
            String cantidad = this.cant_Producto.getText();
            String precio = this.precio_Producto.getText();
            if (tipo.equals("") || nombre.equals("") || cantidad.equals("") || precio.equals(""))
                crearDialogo("¡Error!", "Las casillas no pueden estar vacias.");
            else {
                Producto producto = new Producto(tipo.toUpperCase(), nombre.toUpperCase(), Integer.parseInt(cantidad), Float.parseFloat(precio));
                this.tienda.agregarProducto(producto);
                this.productos.add(producto);
                this.listView.setItems(productos);
                handleCancelarAction(null);
            }
        }catch(Exception e){
            crearDialogo("Error Agregar Producto", e.getMessage());
        }
    }

    @FXML
    private void handleCancelarAction(ActionEvent event) {
        this.nombre_Producto.setText("");
        this.cant_Producto.setText("");
        this.precio_Producto.setText("");
        
        this.agregarProductoTitle.setExpanded(false);
    }

    @FXML
    private void hanldeHacerPedidoAction(ActionEvent event) {
        try{
            String cantidad = this.cant_HacerPedido.getText();
            if (cantidad.equals(""))
                crearDialogo("¡Error!", "Las casillas no pueden estar vacias.");
            else {
                Producto producto = this.listView.getSelectionModel().getSelectedItem();
                if(producto==null)
                    throw new Exception("Seleccione un producto primero.");
                this.tienda.hacerPedido(Integer.parseInt(cantidad), producto);
                this.cant_HacerPedido.setText("");
                this.listView.refresh();
            }
        }catch(Exception e){
            crearDialogo("Error Hacer Pedido", e.getMessage());
        }
    }

    @FXML
    private void handleVenderProductoAction(ActionEvent event) {
        try{
            String cantidad = this.cant_VenderProducto.getText();
            if (cantidad.equals(""))
                crearDialogo("¡Error!", "Las casillas no pueden estar vacias.");
            else {
                Producto producto = this.listView.getSelectionModel().getSelectedItem();
                if(producto==null)
                    throw new Exception("Seleccione un producto primero.");
                this.tienda.venderProducto(Integer.parseInt(cantidad), producto);
                this.cant_VenderProducto.setText("");
                this.listView.refresh();
                if (producto.getCantidad_Disponible() <= producto.getCantidad_Minima()) {
                    throw new Exception("Su producto se está agotando, por favor haga un pedido.");
                }
            }
        }catch(Exception e){
            crearDialogo("Error Vender Producto", e.getMessage());
        }
    }

    @FXML
    private void handleMostrarEstadisticasAction(ActionEvent event) {
        String cadena = this.tienda.mostrarEstadisticas();
        this.textArea.setText(cadena);
    }

    @FXML
    private void handleOrdenarPorPapeleriaAction(ActionEvent event) {
        try{
            LinkedList<Producto> aux = new LinkedList<>();
            Iterator<Producto> it = this.tienda.getProductos().iterator();
            while (it.hasNext()) {
                Producto producto = it.next();
                if (producto != null && producto.getTipo().equals("PAPELERIA")) {
                    aux.add(producto);
                }
            }
            this.productos.setAll(aux);
            this.listView.setItems(productos);
        }catch(Exception e){
            crearDialogo("Error ordenar por papeleria", e.getMessage());
        }
    }

    @FXML
    private void handleOrdenarPorSupermercadoAction(ActionEvent event) {
        try{
            LinkedList<Producto> aux = new LinkedList<>();
            Iterator<Producto> it = this.tienda.getProductos().iterator();
            while (it.hasNext()) {
                Producto producto = it.next();
                if (producto != null && producto.getTipo().equals("SUPERMERCADO")) {
                    aux.add(producto);
                }
            }
            this.productos.setAll(aux);
            this.listView.setItems(productos);
        }catch(Exception e){
            crearDialogo("Error ordenar por supermercado", e.getMessage());
        }
    }

    @FXML
    private void handleOrdenarPorDrogueriaAction(ActionEvent event) {
        try{
            LinkedList<Producto> aux = new LinkedList<>();
            Iterator<Producto> it = this.tienda.getProductos().iterator();
            while (it.hasNext()) {
                Producto producto = it.next();
                if (producto != null && producto.getTipo().equals("DROGUERIA")) {
                    aux.add(producto);
                }
            }
            this.productos.setAll(aux);
            this.listView.setItems(productos);
        }catch(Exception e){
            crearDialogo("Error ordenar por drogueria", e.getMessage());
        }
    }

    @FXML
    private void handleLimpiarAction(ActionEvent event) {
        this.textArea.setText("");
    }

    
    
}