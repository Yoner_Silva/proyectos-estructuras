/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tienda.modelo;

/**
 *
 * @author nelly
 */
public class Producto implements Comparable<Producto>{
    
    private String nombre;
    private String tipo;
    private int cantidad_Disponible;
    private final int cantidad_Minima = 5;
    private float precio_Base;
 
    //Propiedades aparte
    private double precio_Final;
    private int cantidad_Vendidos;
    
    public Producto() {
        
    }
    
    public Producto(String tipo,String nombre, int cantidad, float precio){
        this.tipo = tipo;
        this.nombre = nombre;
        this.cantidad_Disponible = cantidad;
        this.precio_Base = precio;
        this.cantidad_Vendidos = 0;
     }


    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidad_Disponible() {
        return cantidad_Disponible;
    }

    public void setCantidad_Disponible(int cantidad_Disponible) {
        this.cantidad_Disponible = cantidad_Disponible;
    }

    public float getPrecio_Base() {
        return this.precio_Base;
    }

    public void setPrecio(float precio) {
        this.precio_Base = precio;
    }

    public int getCantidad_Minima() {
        return cantidad_Minima;
    }

    public int getCantidad_Vendidos() {
        return cantidad_Vendidos;
    }

    public void setCantidad_Vendidos(int cantidad_Vendidos) {
        this.cantidad_Vendidos = cantidad_Vendidos;
    }

    public double getPrecio_Final() {
        return precio_Final;
    }

    public void setPrecio_Final(double precio_Final) {
        this.precio_Final = precio_Final;
    }
    
    
    
    
    public String imprimir(){
        return this.nombre + " con "+this.cantidad_Vendidos+" unidades vendidas";
    }
    
    @Override
    public String toString(){
        return this.nombre+"     -      "+this.cantidad_Disponible+"      -       $"+String.format("%.2f", this.precio_Final);
    }
    
    
    @Override
    public int compareTo(Producto other) {
        return this.nombre.compareTo(other.getNombre());
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Producto other = (Producto) obj;
        if(this.nombre.equals(other.nombre) && this.tipo.equals(other.tipo))
            return true;
        if(!this.nombre.equals(other.nombre) && this.tipo.equals(other.tipo))
            return false;
        return false;
    }

    
    
}
