/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tienda.modelo;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author jhone
 */
public class Tienda {
    
    private Set<Producto> productos;
    
    //-------------------------------
    private double ganancias;
    private final float margenDeGanancias = 0.05f;
    
    
    public Tienda() {
        this.productos = new TreeSet<>();
    }
    
    public void agregarProducto(Producto producto)throws Exception{
        if(!this.productos.add(producto)){
            throw new Exception("El producto ya existe.");
        }
        calcularPrecioFinal(producto);
    }
    
    public void venderProducto(int cant, Producto producto)throws Exception{
        if(cant > 0){
            if (this.productos.contains(producto)) {
                int cant_Disponibles = producto.getCantidad_Disponible();
                if(producto.getCantidad_Disponible() == 0)
                    throw new Exception("Su producto está agotado.");
                else{
                    if(cant <= cant_Disponibles) {
                        producto.setCantidad_Disponible(cant_Disponibles - cant);
                        producto.setCantidad_Vendidos(cant);
                        
                        this.ganancias += producto.getPrecio_Final()*margenDeGanancias;
                    }  
                }
            } else {
                throw new Exception("El producto no existe, intente agregarlo primero.");
            }
        }else{
            throw new Exception("La cantidad debe ser superior a cero.");
        }
    }
    
    private void calcularGanancias(Producto producto){
        String tipo = producto.getTipo();
        double margen = 0;
        double impuesto = 0;
        switch(tipo){
            case "PAPELERIA":
                final float impuesto_Papeleria = 0.16f;
                impuesto = producto.getPrecio_Base()*impuesto_Papeleria;
                margen = (producto.getPrecio_Base()+impuesto)*this.margenDeGanancias;
                this.ganancias += margen;
                break;
            case "SUPERMERCADO":
                final float impuesto_Supermercado = 0.04f;
                impuesto = producto.getPrecio_Base()*impuesto_Supermercado;
                margen = (producto.getPrecio_Base()+impuesto)*this.margenDeGanancias;
                this.ganancias += margen;
                break;
            case "DROGUERIA":
                final float impuesto_Drogueria = 0.12f;
                impuesto = producto.getPrecio_Base()*impuesto_Drogueria;
                margen = (producto.getPrecio_Base()+impuesto)*this.margenDeGanancias;
                this.ganancias += margen;
                break;
        }
    }
    
    private void calcularPrecioFinal(Producto producto){
        String tipo = producto.getTipo();
        double margen = 0;
        double impuesto = 0;
        switch(tipo){
            case "PAPELERIA":
                final float impuesto_Papeleria = 0.16f;
                impuesto = producto.getPrecio_Base()*impuesto_Papeleria;
                margen = (producto.getPrecio_Base()+impuesto)*this.margenDeGanancias;
                producto.setPrecio_Final(producto.getPrecio_Base()+impuesto+margen);
                break;
            case "SUPERMERCADO":
                final float impuesto_Supermercado = 0.04f;
                impuesto = producto.getPrecio_Base()*impuesto_Supermercado;
                margen = (producto.getPrecio_Base()+impuesto)*this.margenDeGanancias;
                producto.setPrecio_Final(producto.getPrecio_Base()+impuesto+margen);
                break;
            case "DROGUERIA":
                final float impuesto_Drogueria = 0.12f;
                impuesto = producto.getPrecio_Base()*impuesto_Drogueria;
                margen = (producto.getPrecio_Base()+impuesto)*this.margenDeGanancias;
                producto.setPrecio_Final(producto.getPrecio_Base()+impuesto+margen);
                break;
        }
    }
    
    public void hacerPedido(int cant,Producto producto)throws Exception{
        if(cant > 0){
            producto.setCantidad_Disponible(cant+producto.getCantidad_Disponible());
        }else{
            throw new Exception("La cantidad debe ser superior a cero.");
        }
    }
    
    /**
     *  (a) el producto más vendido.
     *  (b) el producto menos vendido.
     *  (c) la cantidad total de dinero obtenido por las ventas de la tienda.
     *  (d) el promedio de ventas de la tienda (valor total de las ventas dividido por el
            número total de unidades vendidas de todos los productos). 
     * @return 
     */
    public String mostrarEstadisticas(){
        Producto menor = null, mayor = null;
        int cant_ProductosVendidos = 0;
        if(!this.productos.isEmpty()){
            Iterator<Producto> it = this.productos.iterator();
            while (it.hasNext()) {
                Producto aux = it.next();
                if (aux != null) {
                    cant_ProductosVendidos += aux.getCantidad_Vendidos();
                    if (menor == null && mayor == null) {
                        menor = aux;
                        mayor = aux;
                    } else {
                        if (menor.getCantidad_Vendidos() > aux.getCantidad_Vendidos()) {
                            menor = aux;
                        }
                        if (mayor.getCantidad_Vendidos() < aux.getCantidad_Vendidos()) {
                            mayor = aux;
                        }
                    }
                }
            }

            double promedio = this.ganancias / cant_ProductosVendidos;
            return "El producto más vendido: " + mayor.imprimir() + "\n"
                    + "El producto menos vendido: " + menor.imprimir() + "\n"
                    + "Las ganancias obtenidas hasta el momento son: " + String.format("%.2f", this.ganancias) + "\n"
                    + "El promedio de las ganancias fueron: " + String.format("%.2f", promedio);
        }
        return "No hay productos agregados.";
    }

    public double getGanancias() {
        return ganancias;
    }

    public Set<Producto> getProductos() {
        return productos;
    }
    
}
