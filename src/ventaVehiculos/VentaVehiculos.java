package ventaVehiculos;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @Yoner Silva
 * @Leiddy Vacca
 */
public class VentaVehiculos {

    //Lista de vehiculos.
    private List<Vehiculo> vehiculos;

    public VentaVehiculos() {
        this.vehiculos = new LinkedList<>();
    }

    /**
     * Ordena la lista de vehiculos por marca, haciendo uso de la interfaz
     * Comparable en la clase Vehiculo.
     */
    public void ordenarPorMarca() {
        Collections.sort(vehiculos);
    }

    //Ordena la lista de vehiculos por cilindraje.
    public void ordenarPorCilindraje() {
        Collections.sort(vehiculos, new Comparator<Vehiculo>() {

            public int compare(Vehiculo v1, Vehiculo v2) {
                if (v1.getCilindraje() < v2.getCilindraje()) {
                    return -1;
                } else if (v1.getCilindraje() > v2.getCilindraje()) {
                    return 1;
                } else {
                    return 0;
                }

            }
        });
    }

    //Ordena la lista de vehiculos por anio de producción.
    public void ordenarPorAnio() {
        Collections.sort(vehiculos, new Comparator<Vehiculo>() {

            public int compare(Vehiculo v1, Vehiculo v2) {
                if (v1.getAnioProduccion() < v2.getAnioProduccion()) {
                    return -1;
                } else if (v1.getAnioProduccion() > v2.getAnioProduccion()) {
                    return 1;
                } else {
                    return 0;
                }

            }
        });
    }

    /**
     * Devuelve una lista con los vehiculos por tipo marca y anio de produccion
     * especificos.
     */
    public List<Vehiculo> buscarVehiculos(String tipoVehiculo, String marca, int anioProduccion) {

        if (!this.vehiculos.isEmpty()) {
            List<Vehiculo> aux = new LinkedList<>();
            Iterator<Vehiculo> it = vehiculos.iterator();
            while (it.hasNext()) {
                Vehiculo x = it.next();
                if ((x.getTipoVehiculo().equals(tipoVehiculo.toUpperCase()) && x.getMarca().equals(marca.toUpperCase())) && (x.getAnioProduccion() == anioProduccion)) {
                    aux.add(x);
                }
            }
            return aux;
        }
        return null;
    }

    /**
     * Agrega un vehiculo siempre y cuando no se encuentre agregado por la
     * placa.
     */
    public boolean addVehiculo(String tipoVehiculo, String marca, String modelo, String placa, int anioProduccion, int numeroEjes, int cilindraje, int valorVehiculo) {
        if (buscarVehiculo(placa) == null) {
            Vehiculo v = new Vehiculo(tipoVehiculo.toUpperCase(), marca.toUpperCase(), modelo.toUpperCase(),
                    placa.toUpperCase(), anioProduccion, numeroEjes, cilindraje, valorVehiculo);
            this.vehiculos.add(v);
            return true;
        }
        return false;
    }

    /**
     * Este método busca la existencia de un vehiculo en la lista por su placa.
     * En caso de que exista, no se podra agregar.
     */
    public Vehiculo buscarVehiculo(String placa) {
        placa = placa.toUpperCase();
        if (!this.vehiculos.isEmpty()) {
            Iterator<Vehiculo> it = vehiculos.iterator();
            while (it.hasNext()) {
                Vehiculo x = it.next();
                if (x.getPlaca().equals(placa)) {
                    return x;
                }
            }
        }
        return null;
    }

    /**
     * Elimina el vehiculo comprado, si y solo sí existe uno con un tipo de
     * vehiculo marca y anio de producción en especifico. De lo contrario
     * devolverá una lista con todos los vehiculos filtrados por esas
     * caracteristicas. De esa manera se busca que el cliente elija cual
     * vehiculo desea comprar.
     */
    public boolean comprarVehiculo(Vehiculo v) {
        return this.vehiculos.remove(buscarVehiculo(v.getPlaca()));
    }

    /**
     * Busca en toda la lista el Vehiculo con anio de produccion más antiguo.
     */
    public List<Vehiculo> buscarVehiculoMasAntiguo() {
        List<Vehiculo> aux = new LinkedList<>();
        Vehiculo masAntiguo = null;
        if (!this.vehiculos.isEmpty()) {
            Iterator<Vehiculo> it = vehiculos.iterator();
            masAntiguo = it.next();
            if (this.vehiculos.size() == 1) {
                aux.add(masAntiguo);
                return aux;
            }

            while (it.hasNext()) {
                Vehiculo x = it.next();

                if (x.compararAnio(masAntiguo) == -1) {
                    masAntiguo = x;
                    if (aux.isEmpty()) {
                        aux.add(masAntiguo);
                    } else {
                        if (!(aux.get(0).compararAnio(masAntiguo) == 0)) {
                            aux.clear();
                            aux.add(masAntiguo);
                        }
                    }
                } else {
                    if (x.compararAnio(masAntiguo) == 0) {
                        if (aux.isEmpty()) {
                            aux.add(masAntiguo);
                            aux.add(x);
                        } else {
                            aux.add(x);
                        }
                    }
                }
            }
        }
        return aux;
    }

    /**
     * Busca en toda la lista el vehiculo con el valor más economico.
     */
    public List<Vehiculo> buscarVehiculoMasEconomico() {
        List<Vehiculo> aux = new LinkedList<>();
        Vehiculo masEconomico = null;
        if (!this.vehiculos.isEmpty()) {
            Iterator<Vehiculo> it = vehiculos.iterator();
            masEconomico = it.next();
            if (this.vehiculos.size() == 1) {
                aux.add(masEconomico);
                return aux;
            }

            while (it.hasNext()) {
                Vehiculo x = it.next();

                if (x.compararValor(masEconomico) == -1) {
                    masEconomico = x;
                    if (aux.isEmpty()) {
                        aux.add(masEconomico);
                    } else {
                        if (!(aux.get(0).compararValor(masEconomico) == 0)) {
                            aux.clear();
                            aux.add(masEconomico);
                        }
                    }
                } else {
                    if (x.compararAnio(masEconomico) == 0) {
                        if (aux.isEmpty()) {
                            aux.add(masEconomico);
                            aux.add(x);
                        } else {
                            aux.add(x);
                        }
                    }
                }
            }
        }
        return aux;
    }

    /**
     * Busca en toda la lista el Vehiculo con el cilindraje mas grande.
     */
    public List<Vehiculo> buscarVehiculoMasPotente() {
        List<Vehiculo> aux = new LinkedList<>();
        Vehiculo masPotente = null;
        if (!this.vehiculos.isEmpty()) {
            Iterator<Vehiculo> it = vehiculos.iterator();
            masPotente = it.next();
            if (this.vehiculos.size() == 1) {
                aux.add(masPotente);
                return aux;
            }

            while (it.hasNext()) {
                Vehiculo x = it.next();

                if (x.compararCilindraje(masPotente) == 1) {
                    masPotente = x;
                    if (aux.isEmpty()) {
                        aux.add(masPotente);
                    } else {
                        if (!(aux.get(0).compararCilindraje(masPotente) == 0)) {
                            aux.clear();
                            aux.add(masPotente);
                        }
                    }
                } else {
                    if (x.compararCilindraje(masPotente) == 0) {
                        if (aux.isEmpty()) {
                            aux.add(masPotente);
                            aux.add(x);
                        } else {
                            aux.add(x);
                        }
                    }
                }
            }
        }
        return aux;
    }

    /**
     * Disminuye el valor del vehiculo un 10% si, y solo si el valor del
     * vehiculo es mayor al valor enviado por parámetro.
     */
    public boolean disminuirPrecio(int valor) {
        boolean valido = false;
        if (!this.vehiculos.isEmpty()) {
            Iterator<Vehiculo> it = vehiculos.iterator();
            while (it.hasNext()) {
                Vehiculo x = it.next();
                if (x.getValorVehiculo() > valor) {
                    x.setValorVehiculo((int) (x.getValorVehiculo() - (x.getValorVehiculo() * 0.1)));
                    valido = true;
                }
            }
        }
        return valido;
    }

    /**
     * Getters and setters
     */
    public List<Vehiculo> getVehiculos() {
        return vehiculos;
    }
}
