/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventaVehiculos.controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import ventaVehiculos.*;

/**
 * FXML Controller class
 *
 * @Yoner Silva
 * @Leiddy Vacca
 */
public class PrincipalController extends Application implements Initializable {
    
    protected static VentaVehiculos vh = new VentaVehiculos();
    private Vehiculo vehiculo;
    
    public static void main(String[] args) {
        launch(args);
    }
    
    
    
    
    
    @Override
    public void start(Stage stage) throws Exception {
        try{      
            Parent root = FXMLLoader.load(getClass().getResource("/ventaVehiculos/interfaz/PrincipalGUI.fxml"));
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("VENTA DE VEHICULOS");
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.show();
        }catch(Exception e){
            System.err.println(e.getMessage());
        }
    }    
    
    @FXML
    private TableView<Vehiculo> tableView;
    @FXML
    private TableColumn<Vehiculo, String> colListaVehiculos;
    @FXML
    private TableColumn<Vehiculo, String> colTipo;
    @FXML
    private TableColumn<Vehiculo, String> colMarca;
    @FXML
    private TableColumn<Vehiculo, String> colModelo;
    @FXML
    private TableColumn<Vehiculo, String> colPlaca;
    @FXML
    private TableColumn<Vehiculo, Integer> colAño;
    @FXML
    private TableColumn<Vehiculo, Integer> colEjes;
    @FXML
    private TableColumn<Vehiculo, Integer> colCilindraje;
    @FXML
    private TableColumn<Vehiculo, Integer> colValor;
    
    @FXML
    private Button ordenarPorAño;
    @FXML
    private Button ordenarPorCilindraje;
    @FXML
    private Button ordenarPorMarca;
    @FXML
    private Button buscarVehiculo;
    @FXML
    private Button AgregarVehiculo;
    @FXML
    private Button comprarVehiculo;
    @FXML
    private Button disminuirPrecio;
    @FXML
    private Button masPotente;
    @FXML
    private Button masEconomico;
    @FXML
    private Button masAntiguo;
    @FXML
    private ComboBox<String> tipoVehiculo;
    @FXML
    private TextField BuscarMarca;
    @FXML
    private TextField BuscarAño;
    @FXML
    private Button BotonBuscar;
    @FXML
    private TextField BuscarPlaca;
    

    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tipoVehiculo.setItems(darValor());
        modificarColumnas();
    }
    
    private void modificarColumnas(){
        this.colTipo.setCellValueFactory(new PropertyValueFactory<Vehiculo, String>("tipoVehiculo"));
        this.colMarca.setCellValueFactory(new PropertyValueFactory<Vehiculo, String>("marca"));
        this.colModelo.setCellValueFactory(new PropertyValueFactory<Vehiculo, String>("modelo"));
        this.colPlaca.setCellValueFactory(new PropertyValueFactory<Vehiculo, String>("placa"));
        this.colAño.setCellValueFactory(new PropertyValueFactory<Vehiculo, Integer>("anioProduccion"));
        this.colEjes.setCellValueFactory(new PropertyValueFactory<Vehiculo, Integer>("numeroEjes"));
        this.colCilindraje.setCellValueFactory(new PropertyValueFactory<Vehiculo, Integer>("cilindraje"));
        this.colValor.setCellValueFactory(new PropertyValueFactory<Vehiculo, Integer>("valorVehiculo"));
    }

    private ObservableList<String> darValor(){
        ObservableList<String> options
                = FXCollections.observableArrayList(
                        "BUS",
                        "AUTOMÓVIL",
                        "CAMIÓN",
                        "MOTO"
                );
        return options;
    } 
    
    private final ListChangeListener<Vehiculo> selectorTablaVehiculo =
            new ListChangeListener<Vehiculo>() {
                @Override
                public void onChanged(ListChangeListener.Change<? extends Vehiculo> c) {
                    ponerVehiculoSeleccionado();
                }
            };
    
    private Vehiculo getTablaVehiculoSeleccionado() {
        if (this.tableView != null) {
            List<Vehiculo> tabla = this.tableView.getSelectionModel().getSelectedItems();
            if (tabla.size() == 1) {
                final Vehiculo seleccion = tabla.get(0);
                return seleccion;
            }
        }
        return null;
    }
    
    private void ponerVehiculoSeleccionado() {
        final Vehiculo v = getTablaVehiculoSeleccionado();
        if (v != null) {
            this.vehiculo = v;
        }
    }
    
    public void crearDialogo(String title,String context){
        Alert dialogo;
        dialogo = new Alert(Alert.AlertType.INFORMATION);
        dialogo.setTitle(title);
        dialogo.setContentText(context);
        dialogo.initStyle(StageStyle.UTILITY);
        java.awt.Toolkit.getDefaultToolkit().beep();
        dialogo.showAndWait();
    }
        

    /**
     * Boton para buscar un vehiculo o varios vehiculos.
     */
    @FXML
    private void handleBuscarVehiculoAction(ActionEvent event)throws Exception{
        try{
            String tipoVehiculo = this.tipoVehiculo.getValue();
            String marca = BuscarMarca.getText();
            String anio = BuscarAño.getText();
            if((marca.equals("")) || (anio.equals("") || (tipoVehiculo.equals("")))){
                crearDialogo("¡Error!","Las casillas no pueden estar vacias.");
            }else{
                List<Vehiculo> lista = this.vh.buscarVehiculos(tipoVehiculo, marca, Integer.parseInt(anio));
                if(lista != null){
                    ObservableList<Vehiculo> aux = FXCollections.observableArrayList(lista);
                    this.tableView.setItems(aux);
                    this.tipoVehiculo.setValue("");
                    this.BuscarMarca.setText("");
                    this.BuscarAño.setText("");
                }else{
                    crearDialogo("Buscar Vehiculo","No se encontraron vehiculos.");
                }              
            }
        }catch(Exception e){
            System.err.println("Error al buscar vehículo. "+e.getMessage());
        }
    }
    
    @FXML
    private void onEnter(ActionEvent event) {
        try{
            String placa = BuscarPlaca.getText();
            if(placa.equals(""))
                crearDialogo("¡Error!","Las casillas no pueden estar vacias.");
            else{
                Vehiculo v = this.vh.buscarVehiculo(placa);
                if(v != null){
                    ObservableList<Vehiculo> aux = FXCollections.observableArrayList(v);
                    this.tableView.setItems(aux);
                    this.BuscarPlaca.setText("");
                }else{
                    crearDialogo("Buscar Vehiculo","No se encontró el vehiculo.");
                }
            }
        }catch(Exception e){
            System.err.println("Error al buscar por placa. "+e.getMessage());
        }
    }

    @FXML
    private void handleActualizarAction(MouseEvent event){
        ObservableList<Vehiculo> aux = FXCollections.observableArrayList(this.vh.getVehiculos());
        this.tableView.setItems(aux);
    }
    
    /**
     * Boton agregar vehiculo
     */
    @FXML
    private void handleAgregarVehiculoAction(ActionEvent event)throws Exception {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/ventaVehiculos/interfaz/interfazAgregar.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("AGREGAR VEHICULO");
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.show();

        } catch (Exception e) {
            System.err.println("2."+e.getMessage());
        }
    }

    /**
     * Boton comprar vehiculo
     */
    @FXML
    private void handleComprarVehiculoAction(ActionEvent event)throws Exception {
        try{
            ponerVehiculoSeleccionado();
            if(this.vehiculo != null){
                boolean comprar = this.vh.comprarVehiculo(this.vehiculo);
                if(comprar){
                    ObservableList<Vehiculo> aux = FXCollections.observableList(vh.getVehiculos());
                    this.tableView.setItems(aux);
                    crearDialogo("Comprar Vehiculo","Se compro el vehículo exitosamente.");
                }
            }else{
                crearDialogo("¡Error!","Seleccione un vehiculo de la tabla.");
            }
   
        }catch(Exception e){
            System.err.println("Error al comprar. "+ e.getMessage());
        }
    }

    /**
     * Boton disminuir un 10% el precio de vehiculos.
     */
    @FXML
    private void handleDisminuirPrecioAction(ActionEvent event)throws Exception {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/ventaVehiculos/interfaz/interfazDisminuir.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("DISMINUIR PRECIO");
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.show();
        } catch (Exception e) {
            System.err.println("Error boton disminuir."+e.getMessage());
        }

    }
     
    
    @FXML
    private void handleOrdenarPorAño(ActionEvent event){          
        try{
            vh.ordenarPorAnio();
            ObservableList<Vehiculo> aux = FXCollections.observableList(vh.getVehiculos());
            this.tableView.setItems(aux);
        }catch(Exception e){
            System.err.println("Error al ordenar por año. "+e.getMessage());
        }
    }
    
    @FXML
    private void handleOrdenarPorMarca(ActionEvent event){
        try{
            vh.ordenarPorMarca();
            ObservableList<Vehiculo> aux = FXCollections.observableList(vh.getVehiculos());
            this.tableView.setItems(aux);
        }catch(Exception e){
            System.err.println("Error al ordenar por marca. "+e.getMessage());
        }     
    }
    
    @FXML
    private void handleOrdenarPorCilindraje(ActionEvent event){
        try {
            vh.ordenarPorCilindraje();
            ObservableList<Vehiculo> aux = FXCollections.observableList(vh.getVehiculos());
            this.tableView.setItems(aux);
        } catch (Exception e) {
            System.err.println("Error al ordenar por cilindraje. " + e.getMessage());
        }
    }
    
    @FXML
    private void handleMasPotenteAction(ActionEvent event){
        try {
            List<Vehiculo> lista = this.vh.buscarVehiculoMasPotente();
            if(!lista.isEmpty()){
                ObservableList<Vehiculo> aux = FXCollections.observableList(lista);
                this.tableView.setItems(aux);
            } else {
                crearDialogo("Mas Potente", "No hay vehiculos.");
            }
        } catch (Exception e) {
            System.err.println("Error al buscar el más potente. " + e.getMessage());
        }
    }
    
    @FXML
    private void handleMasEconomicoAction(ActionEvent event){
        try {          
            List<Vehiculo> lista = this.vh.buscarVehiculoMasEconomico();
            if(!lista.isEmpty()){
                ObservableList<Vehiculo> aux = FXCollections.observableList(lista);
                this.tableView.setItems(aux);
            }else{
                crearDialogo("Mas Economico", "No hay vehiculos.");
            }
        } catch (Exception e) {
            System.err.println("Error al buscar el más economico. " + e.getMessage());
        }
    }
    
    @FXML
    private void handleMasAntiguoAction(ActionEvent event){
        try {
            List<Vehiculo> lista = vh.buscarVehiculoMasAntiguo();
            if(!lista.isEmpty()){
                ObservableList<Vehiculo> aux = FXCollections.observableList(lista);
                this.tableView.setItems(aux);               
            }else{
                crearDialogo("Mas Antiguo", "No hay vehiculos.");
            }
        } catch (Exception e) {
            System.err.println("Error al buscar el más antiguo. " + e.getMessage());
        }
    } 
    
    
    
    public VentaVehiculos getVh() {
        return vh;
        
    }
}
