/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventaVehiculos.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author jhone
 */
public class InterfazDisminuirController extends PrincipalController implements Initializable {

    @FXML
    private TextField DisminuirPrecio;
    @FXML
    private Button botonAceptar;
    @FXML
    private Button botonCancelar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
    private void crearDialogo(String tipo){
        tipo = tipo.toUpperCase();
        Alert dialogo;
        switch(tipo){
            case "CASILLAS VACIAS": 
                dialogo = new Alert(Alert.AlertType.INFORMATION);
                dialogo.setTitle("Â¡Error!");
                dialogo.setContentText("Las casillas no pueden estar vacÃ­as.");
                dialogo.initStyle(StageStyle.UTILITY);
                java.awt.Toolkit.getDefaultToolkit().beep();
                dialogo.showAndWait();
                break;
            case "NO SE PUDO":
                dialogo = new Alert(Alert.AlertType.INFORMATION);
                dialogo.setTitle("Disminuir Precio");
                dialogo.setContentText("No se encontraron vehÃ­culos." + '\n' + "Intente nuevamente. :D");
                java.awt.Toolkit.getDefaultToolkit().beep();
                dialogo.showAndWait();
                break;
            case "SE PUDO":
                dialogo = new Alert(Alert.AlertType.INFORMATION);
                dialogo.setTitle("Disminuir Precio");
                dialogo.setContentText("Proceso Ã‰xitoso");
                java.awt.Toolkit.getDefaultToolkit().beep();
                dialogo.showAndWait();
                break;
        }
        
    }
    
    @FXML
    private void handleBotonAceptarAction(ActionEvent event)throws Exception{
        try{          
            String valor = this.DisminuirPrecio.getText();
            if(valor.equals("")){
                crearDialogo("CASILLAS VACIAS");
            }else{
                boolean valido = super.getVh().disminuirPrecio(Integer.parseInt(valor));
                if(valido){
                    crearDialogo("SE PUDO");
                    Stage stage = (Stage) this.botonAceptar.getScene().getWindow();
                    stage.close();
                }else
                    crearDialogo("NO SE PUDO");
            }
            
        }catch(Exception e){
            System.err.println("Error al disminuir precio. "+e.getMessage());
        }
    }

    @FXML
    private void handleBotonCancelarAction(ActionEvent event) {
        Stage stage = (Stage)this.botonCancelar.getScene().getWindow();
        stage.close();
    }
    
}
