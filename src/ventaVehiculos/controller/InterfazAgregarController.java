/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventaVehiculos.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
/**
 * FXML Controller class
 *
 * @author jhone
 */
public class InterfazAgregarController extends PrincipalController implements Initializable {

    public InterfazAgregarController() {
    }
    
    
    @FXML
    private TextField AgregarModelo;
    @FXML
    private TextField AgregarMarca;
    @FXML
    private ComboBox<String> AgregarTipo;           
    @FXML
    private TextField AgregarAño;
    @FXML
    private TextField AgregarCilindraje;
    @FXML
    private TextField AgregarValor;
    @FXML
    private Button BotonAgregar;
    @FXML
    private TextField AgregarPlaca;
    @FXML
    private TextField AgregarEjes;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        AgregarTipo.setItems(darValor());
    }
    
    private ObservableList<String> darValor(){
        ObservableList<String> options
                = FXCollections.observableArrayList(
                        "BUS",
                        "AUTOMÓVIL",
                        "CAMIÓN",
                        "MOTO"
                );
        return options;
    }
    
    @FXML
    private void handleaAgregarVehiculoAction(ActionEvent event) throws Exception {
        try {
            String tipoVehiculo = this.AgregarTipo.getValue();
            String marca = AgregarMarca.getText();
            String modelo = AgregarModelo.getText();
            String placa = AgregarPlaca.getText();
            String anio = AgregarAño.getText();
            String ejes = AgregarEjes.getText();
            String cilindraje = AgregarCilindraje.getText();
            String valor = AgregarValor.getText();
            
            if ((marca.equals("")) || (anio.equals("") || (tipoVehiculo.equals("")) || (modelo.equals("")) || (cilindraje.equals("")) || (valor.equals("")))) {
                super.crearDialogo("¡Error!", "Las casillas no pueden estar vacías.");
            } else {
                boolean addVehiculo = super.getVh().addVehiculo(tipoVehiculo, marca, modelo, placa, Integer.parseInt(anio),Integer.parseInt(ejes), Integer.parseInt(cilindraje), Integer.parseInt(valor));
                if (addVehiculo){
                    super.crearDialogo("Agregar Vehiculo","El Vehículo con placa "+placa.toUpperCase()+" fue añadido con exito.");
                    Stage stage = (Stage) BotonAgregar.getScene().getWindow();
                    stage.close();
                }else 
                    super.crearDialogo("Agregar Vehiculo","El vehículo no se pudo agregar debido a que ya existe uno con la misma placa."+ '\n' 
                        + "Intente nuevamente. :(");                
            }

        } catch (Exception e) {
            System.err.println("Error al agregar. "+e.getMessage());
        }

    }

}
