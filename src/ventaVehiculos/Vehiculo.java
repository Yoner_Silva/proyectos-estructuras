package ventaVehiculos;

/**
 *
 * @author Yoner Silva
 * @Leiddy Vacca
 */
public class Vehiculo implements Comparable<Vehiculo>{
    private String tipoVehiculo, marca, modelo, placa;
    private int anioProduccion, numeroEjes, cilindraje, valorVehiculo;

    public Vehiculo() {
    }

    
    public Vehiculo(String tipoVehiculo, String marca, String modelo, String placa, int anioProduccion, int numeroEjes, int cilindraje, int valorVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
        this.marca = marca;
        this.modelo = modelo;
        this.placa = placa;
        this.anioProduccion = anioProduccion;
        this.numeroEjes = numeroEjes;
        this.cilindraje = cilindraje;
        this.valorVehiculo = valorVehiculo;
    }
    
    //Compara el cilindraje de this con other.
    public int compararCilindraje(Vehiculo other){
        if(this.cilindraje == other.cilindraje)
            return 0;
        else
            if(this.cilindraje > other.cilindraje )
                return 1;
            else
                return -1;
    }
    
    //Compara la marca de this con other.
    public int compararMarca(Vehiculo other){
        return this.getMarca().compareTo(other.getMarca());
    }
    
    //Compara el anio de this con other.
    public int compararAnio(Vehiculo other){
        if(this.anioProduccion == other.anioProduccion)
            return 0;
        else
            if(this.anioProduccion > other.anioProduccion)
                return 1;
            else
                return -1;
    }
    
    //Compara el valor de this con other.
    public int compararValor(Vehiculo other){
        if(this.valorVehiculo == other.valorVehiculo)
            return 0;
        else
            if(this.valorVehiculo > other.valorVehiculo)
                return 1;
            else
                return -1;
    }

    /*@Override
    public String toString() {
        return "Tipo de Vehículo: "+this.tipoVehiculo+'\n'+
                    "Marca: "+this.marca+'\n'+
                    "Modelo: "+this.modelo+'\n'+
                    "Placa: "+this.placa+'\n'+
                    "Año de Producción: "+this.anioProduccion+'\n'+
                    "Número de Ejes: "+this.numeroEjes+'\n'+
                    "Cilindraje: "+this.cilindraje+'\n'+
                    "Valor: "+this.valorVehiculo;
    }*/
    
    @Override
    public int compareTo(Vehiculo other) {
        return this.getMarca().compareTo(other.getMarca());
    }
    /**
     * Getters & Setters 
     * Class Vehiculo
     * @return;
     */
    
    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public int getAnioProduccion() {
        return anioProduccion;
    }

    public void setAnioProduccion(int anioProduccion) {
        this.anioProduccion = anioProduccion;
    }

    public int getNumeroEjes() {
        return numeroEjes;
    }

    public void setNumeroEjes(int numeroEjes) {
        this.numeroEjes = numeroEjes;
    }

    public int getCilindraje() {
        return cilindraje;
    }

    public void setCilindraje(int cilindraje) {
        this.cilindraje = cilindraje;
    }

    public int getValorVehiculo() {
        return valorVehiculo;
    }

    public void setValorVehiculo(int valorVehiculo) {
        this.valorVehiculo = valorVehiculo;
    }

    
    
    
}
