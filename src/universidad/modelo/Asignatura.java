/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universidad.modelo;

/**
 *
 * @author jhone
 */
public class Asignatura {
    
    private int codigo;
    private String nombre;
    private short numero_Creditos;
    private int cupos_Disponibles;
    
    private Alumno [] alumnos_Matriculados;

    public Asignatura() {
    }


    public Asignatura(int codigo, String nombre, short numero_Creditos, int cupos) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.numero_Creditos = numero_Creditos;
        this.alumnos_Matriculados = new Alumno[cupos];
        this.cupos_Disponibles = cupos;
    }
    
    
    
    public void Matricular(Alumno x)throws Exception{
        if(existeAlumno(x) == null){
            if(hayCupo()){
                for (int i = 0; i < this.alumnos_Matriculados.length; i++) {
                    if (this.alumnos_Matriculados[i] == null) {
                        this.alumnos_Matriculados[i] = x;
                        x.agregarAsignaura(this);
                        this.cupos_Disponibles--;
                        return;
                    }
                }
            }else{
                throw new Exception("No hay cupos para la asignatura: "+nombre);
            }
        }else{
            throw new Exception("El alumno con el DNI "+x.getDNI()+" ya se "+"\n"+"encuentra matriculado.");
        }
    }
    
    public void eliminar_Alumno(Alumno x){
        for (int i = 0; i < alumnos_Matriculados.length; i++) {
            if(alumnos_Matriculados[i] == x){
                alumnos_Matriculados[i] = null;
                this.cupos_Disponibles++;
                return;
            }
        }
    }
    
    public void modificar_Alumno(Alumno x){
        for (int i = 0; i < alumnos_Matriculados.length; i++) {
            if(alumnos_Matriculados[i].getDNI() == x.getDNI()){
                alumnos_Matriculados[i] = x;
                return;
            }
        }
    }
    
    private Alumno existeAlumno(Alumno x){
        for (Alumno alumno : alumnos_Matriculados) {
            if(alumno == x)
                return alumno;
        }
        return null;
    }
    
    private boolean hayCupo(){
        if(this.cupos_Disponibles != 0)
            return true;
        return false;
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public short getNumero_Creditos() {
        return numero_Creditos;
    }

    public void setNumero_Creditos(short numero_Creditos) {
        this.numero_Creditos = numero_Creditos;
    }

    public Alumno[] getAlumnos_Matriculados() {
        return alumnos_Matriculados;
    }

    public int getCupos_Disponibles() {
        return cupos_Disponibles;
    }
    
    public void setCupos_Disponibles(int cupos)throws Exception{
        int cuposTotales = this.alumnos_Matriculados.length;
        int resta = cuposTotales - this.cupos_Disponibles;
        
        int operacion = cupos - resta;
        if( operacion >= 0){
            Alumno [] aux = new Alumno[cupos];
            this.cupos_Disponibles = operacion;
            byte cont = 0;
            for (Alumno alumno : alumnos_Matriculados) {
                if(alumno != null){
                    for (int i = 0; i < aux.length; i++) {
                        if(aux[i] == null){
                            aux[i] = alumno;
                            cont++;
                            break;
                        }
                    }
                }
                if(cont == resta)
                    break;
            }
            this.alumnos_Matriculados = aux;
        }else{
            throw new Exception("Los cupos ha modificar deben ser mayor a la \n"
                    + "cantidad de alumnos matriculados, en total son: "+ resta);
        }
    }
    
}
