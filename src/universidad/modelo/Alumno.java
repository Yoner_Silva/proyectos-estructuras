/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universidad.modelo;

import java.util.LinkedList;

/**
 *
 * @author jhone
 */
public class Alumno implements Comparable<Alumno>{
    
    private int DNI;
    private String nombre;
    private String direccion_Vivienda;
    private String email;
    private int edad;
    
    private LinkedList<Asignatura> asignaturas_Matriculadas;
    private short creditos_Matriculados;
    protected final int total_Creditos_Permitidos = 20;
    
    public Alumno() {
    }

    public Alumno(int DNI, String nombre, String direccion_Vivienda, String email, int edad) {
        this.DNI = DNI;
        this.nombre = nombre;
        this.direccion_Vivienda = direccion_Vivienda;
        this.email = email;
        this.edad = edad;
        this.asignaturas_Matriculadas = new LinkedList<>();
        this.creditos_Matriculados = 0;
    }
    
    public void agregarAsignaura(Asignatura x)throws Exception{
        if(existeAsignatura(x) == null){
            if(sePuede(x)){
                this.asignaturas_Matriculadas.add(x);
                this.creditos_Matriculados += x.getNumero_Creditos();
            }else{
                throw new Exception("Los creditos matriculados superan el permitido.");
            }
        }else{
            throw new Exception("La asignatura con el codigo "+x.getCodigo()+" ya se "+"\n"+"encuentra agregada.");
        }
    }
    
    public void eliminar_Asignatura(Asignatura x){
        this.asignaturas_Matriculadas.remove(x);
        this.creditos_Matriculados -= x.getNumero_Creditos();
    }
    
    public void modificar_Alumno(Asignatura x){
        for (Asignatura asignatura : asignaturas_Matriculadas) {
            if(asignatura.getCodigo() == x.getCodigo()){
                asignatura = x;
                return;
            }
        }
    }
    
    private Asignatura existeAsignatura(Asignatura x){
        for (Asignatura asignatura : asignaturas_Matriculadas) {
            if(asignatura == x)
                return asignatura;
        }
        return null;
    }
    
    private boolean sePuede(Asignatura x){
        int suma = this.creditos_Matriculados + x.getNumero_Creditos();
        if(suma <= this.total_Creditos_Permitidos)
            return true;
        return false;
    }

    public int getDNI() {
        return DNI;
    }

    public void setDNI(int DNI) {
        this.DNI = DNI;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion_Vivienda() {
        return direccion_Vivienda;
    }

    public void setDireccion_Vivienda(String direccion_Vivienda) {
        this.direccion_Vivienda = direccion_Vivienda;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public LinkedList<Asignatura> getAsignaturas_Matriculadas() {
        return asignaturas_Matriculadas;
    }

    public int getCreditos_Matriculados() {
        return creditos_Matriculados;
    }

    public void setCreditos_Matriculados(short creditos_Matriculados) {
        this.creditos_Matriculados = creditos_Matriculados;
    }

    @Override
    public int compareTo(Alumno other) {
        return this.getNombre().compareTo(other.getNombre());
    }
    
    
}
