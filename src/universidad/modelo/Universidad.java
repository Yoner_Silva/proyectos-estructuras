/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universidad.modelo;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Hashtable;

/**
 *
 * @author jhone
 */
public class Universidad {

    private Hashtable<Integer,Asignatura> asignaturas;
    private Hashtable<Integer,Alumno> alumnos;
    
    public Universidad() {
        asignaturas = new Hashtable<>();
        alumnos = new Hashtable<>();
    }
      
    public LinkedList<Asignatura> matricularAlumno(Alumno alumno, int codigo)throws Exception{
        if(alumno != null){
            if(!this.asignaturas.isEmpty()){
                Asignatura asignatura = this.asignaturas.get(codigo);
                if (asignatura != null) {
                    asignatura.Matricular(alumno);
                    return alumno.getAsignaturas_Matriculadas();
                } else {
                    throw new Exception("No hay ninguna asignatura con el codigo: " + codigo);
                }
            }else{
                throw new Exception("No hay ninguna asignatura agregada hasta el momento. \n"
                        + "Regrese y agregue una asignatura.");
            }
        }
        return null;
    }
    
    public LinkedList<Alumno> alumnosMatriculados(int codigo)throws Exception{
        Asignatura asignatura = this.asignaturas.get(codigo);
        if(asignatura != null){
            LinkedList<Alumno> lista = new LinkedList<>();
            for (Alumno alumno : asignatura.getAlumnos_Matriculados()) {
                if(alumno != null)
                    lista.add(alumno);
            }
            Collections.sort(lista);
            if(lista.isEmpty()){
                return null;
            }else{
                return lista;
            }
        }else{
            throw new Exception("La asignatura no se encontró.");
        }
    }
    
    public Alumno agregarAlumno(int DNI, String nombre, String direccion_Vivienda, String email, int edad)throws Exception{
        Alumno alumno = new Alumno(DNI, nombre, direccion_Vivienda, email, edad);
        if(buscarAlumno(alumno) == null){
            this.alumnos.put(DNI, alumno);
            return alumno;
        }else{
            throw new Exception("El alumno ya existe.");
        }
    }
    
    public void eliminarAlumno(Alumno x){
        if(buscarAlumno(x) != null){
            for (Asignatura asignatura : x.getAsignaturas_Matriculadas()) {
                asignatura.eliminar_Alumno(x);
            }
            this.alumnos.remove(x.getDNI());
        }
    }
    
    public void modificarAlumno(Alumno x){
        this.alumnos.replace(x.getDNI(), x);
    }
    
    public Alumno consultarAlumno(int DNI)throws Exception{
        Alumno alumno = this.alumnos.get(DNI);
        if(alumno != null)
            return alumno;
        else{
            throw new Exception("No se encontró el alumno con el DNI: "+ DNI);
        }
    }
    
    private Alumno buscarAlumno(Alumno x){
        return this.alumnos.get(x.getDNI());
    }
    
    public Asignatura agregarAsignatura(int codigo, String nombre, short numero_Creditos, int cupos)throws Exception{
        //if(cupos >= 20 && cupos <=40){
            Asignatura asignatura = new Asignatura(codigo, nombre, numero_Creditos, cupos);
            if (buscarAsignatura(asignatura) == null) {
                this.asignaturas.put(codigo, asignatura);
                return asignatura;
            } else {
                throw new Exception("la asignatura ya existe.");
            }
        //}else{
            //throw new Exception("Los cupos permitidos deben estar entre 20 y 40.");
        //}
    }
    
    public void eliminarAsignatura(Asignatura x){
        for (Alumno alumno : x.getAlumnos_Matriculados()) {
            if(alumno != null){
                if(!alumno.getAsignaturas_Matriculadas().isEmpty())
                    alumno.eliminar_Asignatura(x);
            }
        }
        this.asignaturas.remove(x.getCodigo());
    }
    
    public void modificarAsignatura(Asignatura x){
        this.asignaturas.replace(x.getCodigo(), x);
    }
    
    public Asignatura consultarAsignatura(int codigo)throws Exception{
        Asignatura asignatura = this.asignaturas.get(codigo);
        if(asignatura != null)
            return asignatura;
        else{
            throw new Exception("No se encontró la asignatura con el codigo: "+codigo);
        }
    }
    
    private Asignatura buscarAsignatura(Asignatura x){
        return this.asignaturas.get(x.getCodigo());
    }
}
