/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universidad.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import universidad.modelo.Alumno;
import universidad.modelo.Asignatura;

/**
 * FXML Controller class
 *
 * @author jhone
 */
public class MatricularController implements Initializable {
    
    private PrincipalUniversidadController principal;
    private Alumno alumno;
    
    @FXML
    private TableView<Asignatura> tableView;
    @FXML
    private TableColumn<Asignatura, Integer> colCodigo;
    @FXML
    private TableColumn<Asignatura, String> colNombreAsignatura;
    @FXML
    private TableColumn<Asignatura, Short> colCreditos;
    @FXML
    private TextField codigoAsignatura;
    @FXML
    private Button matricular;
    @FXML
    private Button cancelar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.tableView.setSelectionModel(null);
        this.colCodigo.setCellValueFactory(new PropertyValueFactory<Asignatura,Integer>("codigo"));
        this.colNombreAsignatura.setCellValueFactory(new PropertyValueFactory<Asignatura,String>("nombre"));
        this.colCreditos.setCellValueFactory(new PropertyValueFactory<Asignatura,Short>("numero_Creditos"));
    }    

    @FXML
    private void handleMatricularAction(ActionEvent event) {
        try{
            String codigo = this.codigoAsignatura.getText();

            if (codigo.equals("")) {
                principal.crearDialogo("¡Error!", "La casilla de codigo no puede estar vacia.");
            } else {
                principal.getUniversidad().matricularAlumno(this.alumno, Integer.parseInt(codigo));
                ObservableList<Asignatura> aux = FXCollections.observableArrayList(alumno.getAsignaturas_Matriculadas());
                this.tableView.setItems(aux);
                principal.getTableView2().refresh();
                principal.getTableView1().refresh();
                principal.crearDialogo("Matricular Alumno", "Proceso exitoso.");
            }
        }catch(Exception e){
            principal.crearDialogo("¡Error Matricular Alumno!", e.getMessage());
        }
    }
    
    public void recibirParametros(PrincipalUniversidadController x, Alumno alumno){
        this.principal = x;
        this.alumno = alumno;
    }

    @FXML
    private void handleCancelarAction(ActionEvent event) {
        Stage stage = (Stage)this.cancelar.getScene().getWindow();
        stage.close();
    }
    
}
