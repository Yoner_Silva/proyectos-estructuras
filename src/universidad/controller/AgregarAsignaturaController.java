/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universidad.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import universidad.modelo.Asignatura;

/**
 * FXML Controller class
 *
 * @author jhone
 */
public class AgregarAsignaturaController implements Initializable {

    private PrincipalUniversidadController principal;
    
    @FXML
    private TextField codigo;
    @FXML
    private TextField nombreAsignatura;
    @FXML
    private TextField creditos;
    @FXML
    private Button guardarAsignatura;
    @FXML
    private Button cancelarAsignatura;
    @FXML
    private TextField cupos;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void handleGuardarAction(ActionEvent event) {
        try{
            String nombre = this.nombreAsignatura.getText();
            String codigo = this.codigo.getText();
            String creditos = this.creditos.getText();
            String cupos = this.cupos.getText();

            if (nombre.equals("") || codigo.equals("") || creditos.equals("")) {
                principal.crearDialogo("¡Error!", "Las casillas no pueden estar vacias.");
            } else {
                if(cupos.equals(""))
                    cupos = String.valueOf(30);
                
                Asignatura asignatura = principal.getUniversidad().agregarAsignatura(Integer.parseInt(codigo), nombre, Short.parseShort(creditos), Integer.parseInt(cupos));
                principal.recibirAsignatura(asignatura);
                Stage stage = (Stage)this.guardarAsignatura.getScene().getWindow();
                stage.close();
            }
        }catch(Exception e){
            principal.crearDialogo("¡Error Agregar Asignatura!", e.getMessage());
        }
    }

    @FXML
    private void handleCancelarAction(ActionEvent event) {
        Stage stage = (Stage)this.cancelarAsignatura.getScene().getWindow();
        stage.close();
    }
    
    public void recibirParametro(PrincipalUniversidadController x){
        this.principal = x;
    }
    
}
