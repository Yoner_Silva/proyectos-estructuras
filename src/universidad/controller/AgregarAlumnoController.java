/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universidad.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import universidad.modelo.Alumno;

/**
 * FXML Controller class
 *
 * @author jhone
 */
public class AgregarAlumnoController implements Initializable {
    
    private PrincipalUniversidadController principal;

    @FXML
    private TextField nombresApellidos;
    @FXML
    private TextField dniAlumno;
    @FXML
    private TextField direccion;
    @FXML
    private TextField email;
    @FXML
    private TextField edad;
    @FXML
    private Button guardarAlumno;
    @FXML
    private Button cancelarAlumno;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    


    public void recibirParametros(PrincipalUniversidadController x) {
        this.principal = x;
    }

    @FXML
    private void handleGuardarAction(ActionEvent event) {
        try{
            String nombre = this.nombresApellidos.getText();
            String DNI_Alumno = this.dniAlumno.getText();
            String direccion = this.direccion.getText();
            String email = this.email.getText();
            String edad = this.edad.getText();

            if (nombre.equals("") || DNI_Alumno.equals("") || direccion.equals("") || email.equals("") || edad.equals("")) {
                principal.crearDialogo("¡Error!", "Las casillas no pueden estar vacias.");
            } else {
                Alumno alumno = this.principal.getUniversidad().agregarAlumno(Integer.parseInt(DNI_Alumno), nombre, direccion, email, Integer.parseInt(edad));
                principal.recibirAlumno(alumno);
                Stage stage = (Stage)this.guardarAlumno.getScene().getWindow();
                stage.close();
            }
        }catch(Exception e){
            principal.crearDialogo("¡Error Agregar Alumno!", e.getMessage());
        }
    }

    @FXML
    private void handleCancelarAction(ActionEvent event) {
        Stage stage = (Stage)this.cancelarAlumno.getScene().getWindow();
        stage.close();
    }
    
}
