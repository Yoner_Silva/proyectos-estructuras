/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universidad.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import universidad.modelo.Alumno;

/**
 * FXML Controller class
 *
 * @author jhone
 */
public class ModificarAlumnoController implements Initializable {
    
    private PrincipalUniversidadController principal;
    private Alumno alumno;
    
    @FXML
    private TextField nombresApellidos;
    @FXML
    private TextField direccion;
    @FXML
    private TextField email;
    @FXML
    private TextField edad;
    @FXML
    private Button modificarAlumno;
    @FXML
    private Button cancelarAlumno;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void handleModificarAction(ActionEvent event) {
        try{
            String nombre = this.nombresApellidos.getText();
            String direccion = this.direccion.getText();
            String email = this.email.getText();
            String edad = this.edad.getText();
                if(!nombre.equals("") || !direccion.equals("") || !email.equals("") || !edad.equals("")){
                    if (!nombre.equals(""))
                        this.alumno.setNombre(nombre);
                    if (!direccion.equals("")) 
                        this.alumno.setDireccion_Vivienda(direccion);
                    if (!email.equals(""))
                        this.alumno.setEmail(email);
                    if (!edad.equals(""))
                        this.alumno.setEdad(Integer.parseInt(edad));

                    this.principal.getUniversidad().modificarAlumno(alumno);
                    this.principal.getTableView1().refresh();
                    this.principal.crearDialogo("Modificar Alumno", "Proceso exitoso.");
                    Stage stage = (Stage) this.modificarAlumno.getScene().getWindow();
                    stage.close();
                }else{
                    throw new Exception("Indique que desea modificar.");
                }
            
        }catch(Exception e){
            this.principal.crearDialogo("¡Error Modificar Alumno!", e.getMessage());
        }
    }

    @FXML
    private void handleCancelarAction(ActionEvent event) {
        Stage stage = (Stage)this.cancelarAlumno.getScene().getWindow();
        stage.close();
    }
    
    public void recibirParametros(PrincipalUniversidadController x, Alumno y){
        this.principal = x;
        this.alumno = y;
    }
    
}
