/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universidad.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import universidad.modelo.Asignatura;

/**
 * FXML Controller class
 *
 * @author jhone
 */
public class ModificarAsignaturaController implements Initializable {

    private PrincipalUniversidadController principal;
    private Asignatura asignatura;
    
    @FXML
    private TextField nombreAsignatura;
    @FXML
    private TextField creditos;
    @FXML
    private Button modificarAsignatura;
    @FXML
    private Button cancelarAsignatura;
    @FXML
    private TextField cupos;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void handleModificarAction(ActionEvent event) {
        try{
            String nombre = this.nombreAsignatura.getText();
            String creditos = this.creditos.getText();
            String cupos = this.cupos.getText();
            if(!nombre.equals("") || !creditos.equals("") || !cupos.equals("")){
                if(!nombre.equals(""))
                    this.asignatura.setNombre(nombre);
                if(!creditos.equals(""))
                    this.asignatura.setNumero_Creditos(Short.valueOf(creditos));
                if(!cupos.equals(""))
                    this.asignatura.setCupos_Disponibles(Integer.parseInt(cupos));
                
                this.principal.getUniversidad().modificarAsignatura(asignatura);
                this.principal.getTableView2().refresh();
                this.principal.crearDialogo("Modificar Asignatura", "Proceso exitoso.");
                Stage stage = (Stage) this.modificarAsignatura.getScene().getWindow();
                stage.close();
            }else{
                throw new Exception("Indique que desea modificar.");
            }
            
        }catch(Exception e){
            this.principal.crearDialogo("¡Error Modificar Asignatura!", e.getMessage());
        }
    }

    @FXML
    private void handleCancelarAction(ActionEvent event) {
        Stage stage = (Stage)this.cancelarAsignatura.getScene().getWindow();
        stage.close();
    }
    
    public void recibirParametros(PrincipalUniversidadController x, Asignatura y){
        this.principal = x;
        this.asignatura = y;
    }
    
    
}
