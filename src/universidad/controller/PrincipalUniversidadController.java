/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universidad.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import universidad.modelo.Alumno;
import universidad.modelo.Asignatura;
import universidad.modelo.Universidad;

/**
 * FXML Controller class
 *
 * @author jhone
 */
public class PrincipalUniversidadController extends Application implements Initializable {

    private static Universidad universidad = new Universidad();
    
    public static void main(String[] args) {
        launch(args);
    }
   
    @Override
    public void start(Stage stage){
        try{      
            Parent root = FXMLLoader.load(getClass().getResource("/universidad/vista/PrincipalUniversidad.fxml"));
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.setTitle("GESTION DE INFORMACION");
            stage.show();
        }catch(Exception e){
            System.err.println(e.getCause());
        }
    }
    
    @FXML
    private Button agregarAlumno;
    @FXML
    private Button eliminarAlumno;
    @FXML
    private Button modificarAlumno;
    @FXML
    private Button consultarAlumno;
    @FXML
    private TableView<Alumno> tableView;
    @FXML
    private TableColumn<Alumno, Integer> colDni;
    @FXML
    private TableColumn<Alumno, String> colNombreAlumno;
    @FXML
    private TableColumn<Alumno, String> colDireccion;
    @FXML
    private TableColumn<Alumno, String> colEmail;
    @FXML
    private TableColumn<Alumno, Integer> colEdad;
    @FXML
    private TableColumn<Alumno, Short> colCreditosAlumno;
    @FXML
    private Button matricular;
    @FXML
    private Button consultarAlsignatura;
    @FXML
    private Button modificarAsignatura;
    @FXML
    private Button eliminarAsignatura;
    @FXML
    private Button agregarAsignatura;
    @FXML
    private TableView<Asignatura> tableView2;
    @FXML
    private TableColumn<Asignatura, Integer> colCodigo;
    @FXML
    private TableColumn<Asignatura, String> colNombreAsignatura;
    @FXML
    private TableColumn<Asignatura, Short> colCreditosAsignatura;
    @FXML
    private TableColumn<Asignatura, Integer> colCupos;
    
    private ObservableList<Alumno> listaAlumnos;
    private ObservableList<Asignatura> listaAsignaturas;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.tableView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        this.colDni.setCellValueFactory(new PropertyValueFactory<Alumno,Integer>("DNI"));
        this.colNombreAlumno.setCellValueFactory(new PropertyValueFactory<Alumno,String>("nombre"));
        this.colDireccion.setCellValueFactory(new PropertyValueFactory<Alumno,String>("direccion_Vivienda"));
        this.colEmail.setCellValueFactory(new PropertyValueFactory<Alumno,String>("email"));
        this.colEdad.setCellValueFactory(new PropertyValueFactory<Alumno,Integer>("edad"));
        this.colCreditosAlumno.setCellValueFactory(new PropertyValueFactory<Alumno,Short>("creditos_Matriculados"));
            
        this.tableView2.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        this.colCodigo.setCellValueFactory(new PropertyValueFactory<Asignatura,Integer>("codigo"));
        this.colNombreAsignatura.setCellValueFactory(new PropertyValueFactory<Asignatura,String>("nombre"));
        this.colCreditosAsignatura.setCellValueFactory(new PropertyValueFactory<Asignatura,Short>("numero_Creditos"));
        this.colCupos.setCellValueFactory(new PropertyValueFactory<Asignatura,Integer>("cupos_Disponibles"));
        
        this.listaAlumnos = FXCollections.observableArrayList();
        this.listaAsignaturas = FXCollections.observableArrayList();
    }    
    
    public void crearDialogo(String title,String context){
        Alert dialogo;
        dialogo = new Alert(Alert.AlertType.INFORMATION);
        dialogo.setTitle(title);
        dialogo.setContentText(context);
        dialogo.initStyle(StageStyle.UTILITY);
        java.awt.Toolkit.getDefaultToolkit().beep();
        dialogo.showAndWait();
    }

    @FXML
    private void handleAgregarAlumnoAction(ActionEvent event) {
        try{      
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/universidad/vista/AgregarAlumno.fxml"));
            Parent root = (Parent)loader.load();
            AgregarAlumnoController controlador = (AgregarAlumnoController)loader.getController();
            
            controlador.recibirParametros(this);
            
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.setTitle("AGREGAR ALUMNO");
            stage.show();
        }catch(Exception e){
            crearDialogo("¡Error Agregar Alumno!", e.getMessage());
        }
    }
    
    public void recibirAlumno(Alumno alumno){
        if(this.listaAlumnos.isEmpty()){
            this.listaAlumnos = FXCollections.observableArrayList(alumno);
            this.tableView.setItems(listaAlumnos);
        }else{
            this.listaAlumnos.add(alumno);  
            this.tableView.setItems(listaAlumnos);
        }
    }

    @FXML
    private void handleEliminarAlumnoAction(ActionEvent event) {
        try{      
            Alumno alumno = this.tableView.getSelectionModel().getSelectedItem();
            if(alumno != null){
                this.universidad.eliminarAlumno(alumno);
                this.listaAlumnos.remove(alumno);
                this.tableView.setItems(this.listaAlumnos);
                this.tableView2.refresh();
            }else{
                throw new Exception("Seleccione un alumno primero.");
            }
        }catch(Exception e){
            crearDialogo("¡Error Eliminar Alumno!", e.getMessage());
        }
    }

    @FXML
    private void handleModificarAlumnoAction(ActionEvent event) {
        try{      
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/universidad/vista/ModificarAlumno.fxml"));
            Parent root = (Parent)loader.load();
            ModificarAlumnoController controlador = (ModificarAlumnoController)loader.getController();
            
            Alumno alumno = this.tableView.getSelectionModel().getSelectedItem();
            if(alumno == null)
                throw new Exception("Seleccione un alumno de la tabla primero.");
            
            controlador.recibirParametros(this,alumno);
            
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.setTitle("MODIFICAR ALUMNO");
            stage.show();
        }catch(Exception e){
            crearDialogo("¡Error Modificar Alumno!", e.getMessage());
        }
        
    }

    @FXML
    private void handleConsultarAlumnoAction(ActionEvent event) {
        try{      
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/universidad/vista/ConsultarAlumno.fxml"));
            Parent root = (Parent)loader.load();
            ConsultarAlumnoController controlador = (ConsultarAlumnoController)loader.getController();
            
            controlador.recibirParametro(this);
            
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.setTitle("CONSULTAR ALUMNO");
            stage.show();
        }catch(Exception e){
            crearDialogo("¡Error Consultar Alumno!", e.getMessage());
        }
    }

    @FXML
    private void handleMatricularAction(ActionEvent event) {
        try{      
            Alumno alumno = this.tableView.getSelectionModel().getSelectedItem();
            if(alumno != null){
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/universidad/vista/Matricular.fxml"));
                Parent root = (Parent) loader.load();
                MatricularController controlador = (MatricularController) loader.getController();

                controlador.recibirParametros(this, alumno);

                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setScene(scene);
                stage.setResizable(false);
                stage.setFullScreen(false);
                stage.setTitle("MATRICULAR ALUMNO");
                stage.show();
            }else{
                throw new Exception("Seleccione un alumno primero.");
            }
        }catch(Exception e){
            crearDialogo("¡Error Matricular Alumno!", e.getMessage());
        }
    }

    @FXML
    private void handleConsultarAsignaturaAction(ActionEvent event) {
        try{      
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/universidad/vista/ConsultarAsignatura.fxml"));
            Parent root = (Parent)loader.load();
            ConsultarAsignaturaController controlador = (ConsultarAsignaturaController)loader.getController();
            
            controlador.recibirParametro(this);
            
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.setTitle("CONSULTAR ASIGNATURA");
            stage.show();
        }catch(Exception e){
            crearDialogo("¡Error Consultar Asignatura!", e.getMessage());
        }
    }

    @FXML
    private void handleModificarAsignaturaAction(ActionEvent event) {
        try{      
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/universidad/vista/ModificarAsignatura.fxml"));
            Parent root = (Parent)loader.load();
            ModificarAsignaturaController controlador = (ModificarAsignaturaController)loader.getController();
            
            Asignatura asignatura = this.tableView2.getSelectionModel().getSelectedItem();
            if(asignatura == null)
                throw new Exception("Seleccione una asignatura de la tabla primero.");
            
            controlador.recibirParametros(this,asignatura);
            
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.setTitle("MODIFICAR ASIGNATURA");
            stage.show();
        }catch(Exception e){
            crearDialogo("¡Error Modificar Asignatura!", e.getMessage());
        }
    }

    @FXML
    private void handleEliminarAsignaturaAction(ActionEvent event) {
        try{      
            Asignatura asignatura = this.tableView2.getSelectionModel().getSelectedItem();
            if(asignatura != null){
                this.universidad.eliminarAsignatura(asignatura);
                this.listaAsignaturas.remove(asignatura);
                this.tableView2.setItems(this.listaAsignaturas);
                this.tableView.refresh();
            }else{
                throw new Exception("Seleccione una asignatura primero.");
            }
        }catch(Exception e){
            crearDialogo("¡Error Eliminar Asignatura!", e.getMessage());
        }
    }

    @FXML
    private void handleAgregarAsignaturaAction(ActionEvent event) {
        try{      
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/universidad/vista/AgregarAsignatura.fxml"));
            Parent root = (Parent)loader.load();
            AgregarAsignaturaController controlador = (AgregarAsignaturaController)loader.getController();
            
            controlador.recibirParametro(this);
            
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.setTitle("AGREGAR ASIGNATURA");
            stage.show();
        }catch(Exception e){
            crearDialogo("¡Error Agregar Asignatura!", e.getMessage());
        }
    }
    
    public void recibirAsignatura(Asignatura asignatura){
        if(this.listaAsignaturas.isEmpty()){
            this.listaAsignaturas = FXCollections.observableArrayList(asignatura);
            this.tableView2.setItems(listaAsignaturas);
        }else{
            this.listaAsignaturas.add(asignatura);  
            this.tableView2.setItems(listaAsignaturas);
        }
    }

   
    /**
     * GETTERS
     */
    
    public Universidad getUniversidad() {
        return universidad;
    }
    
    public TableView<Asignatura> getTableView2(){
        return this.tableView2;
    }
    
    public TableView<Alumno> getTableView1(){
        return this.tableView;
    }
}
