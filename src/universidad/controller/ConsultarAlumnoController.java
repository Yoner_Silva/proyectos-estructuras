/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universidad.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import universidad.modelo.Alumno;
import universidad.modelo.Asignatura;

/**
 * FXML Controller class
 *
 * @author jhone
 */
public class ConsultarAlumnoController implements Initializable {
    private PrincipalUniversidadController principal;
    
    @FXML
    private TableView<Asignatura> tableView;
    @FXML
    private TableColumn<Asignatura, Integer> colCodigo;
    @FXML
    private TableColumn<Asignatura, String> colNombre;
    @FXML
    private TableColumn<Asignatura, Short> colCreditos;
    @FXML
    private TextField edad;
    @FXML
    private TextField email;
    @FXML
    private TextField direccion;
    @FXML
    private TextField dniAlumno;
    @FXML
    private TextField nombresApellidos;
    @FXML
    private TextField dniAlumnoConsultar;
    @FXML
    private Button buscar;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.tableView.setSelectionModel(null);
        this.colCodigo.setCellValueFactory(new PropertyValueFactory<Asignatura,Integer>("codigo"));
        this.colNombre.setCellValueFactory(new PropertyValueFactory<Asignatura,String>("nombre"));
        this.colCreditos.setCellValueFactory(new PropertyValueFactory<Asignatura,Short>("numero_Creditos"));
    }    

    @FXML
    private void handleBuscarAction(ActionEvent event) {
        try{
            String DNI = this.dniAlumnoConsultar.getText();

            if (DNI.equals("")) {
                principal.crearDialogo("¡Error!", "La casilla de DNI no puede estar vacia.");
            } else {
                Alumno alumno = principal.getUniversidad().consultarAlumno(Integer.parseInt(DNI));
                this.nombresApellidos.setText(alumno.getNombre());
                this.dniAlumno.setText(String.valueOf(alumno.getDNI()));
                this.direccion.setText(alumno.getDireccion_Vivienda());
                this.email.setText(alumno.getEmail());
                this.edad.setText(String.valueOf(alumno.getEdad()));
                ObservableList<Asignatura> aux = FXCollections.observableArrayList(alumno.getAsignaturas_Matriculadas());
                this.tableView.setItems(aux);
                principal.crearDialogo("Consultar Alumno", "Busqueda exitosa.");
            }
        }catch(Exception e){
            principal.crearDialogo("¡Error Consultar Alumno!", e.getMessage());
        }
    }
    
    public void recibirParametro(PrincipalUniversidadController x){
        this.principal = x;
    }
    
}
