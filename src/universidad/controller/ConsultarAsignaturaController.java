/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universidad.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import universidad.modelo.Alumno;
import universidad.modelo.Asignatura;

/**
 * FXML Controller class
 *
 * @author jhone
 */
public class ConsultarAsignaturaController implements Initializable {
    
    private PrincipalUniversidadController principal;
    
    @FXML
    private TableView<Alumno> tableView;
    @FXML
    private TableColumn<Alumno, Integer> colDni;
    @FXML
    private TableColumn<Alumno, String> colNombre;
    @FXML
    private TextField creditos;
    @FXML
    private TextField nombre;
    @FXML
    private TextField codigo;
    @FXML
    private TextField cupos;
    @FXML
    private TextField codigoConsultar;
    @FXML
    private Button buscar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.tableView.setSelectionModel(null);
        this.colDni.setCellValueFactory(new PropertyValueFactory<Alumno,Integer>("DNI"));
        this.colNombre.setCellValueFactory(new PropertyValueFactory<Alumno,String>("nombre"));
    }    

    @FXML
    private void handleBuscarAction(ActionEvent event) {
        try{
            String codigo = this.codigoConsultar.getText();

            if (codigo.equals("")) {
                principal.crearDialogo("¡Error!", "La casilla de codigo no puede estar vacia.");
            } else {
                Asignatura asignatura = principal.getUniversidad().consultarAsignatura(Integer.parseInt(codigo));
                this.codigo.setText(String.valueOf(asignatura.getCodigo()));
                this.nombre.setText(asignatura.getNombre());
                this.creditos.setText(String.valueOf(asignatura.getNumero_Creditos()));
                this.cupos.setText(String.valueOf(asignatura.getCupos_Disponibles()));
                ObservableList<Alumno> aux = FXCollections.observableArrayList(asignatura.getAlumnos_Matriculados());
                this.tableView.setItems(aux);
                principal.crearDialogo("Consultar Asignatura", "Busqueda exitosa.");
            }
        }catch(Exception e){
            principal.crearDialogo("¡Error Consultar Asignatura!", e.getMessage());
        }
    }
    
    public void recibirParametro(PrincipalUniversidadController x){
        this.principal = x;
    }
}
