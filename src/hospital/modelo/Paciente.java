package hospital.modelo;
/**
 * 
 */
public class Paciente implements Comparable<Paciente>{
    private String nombre;
    private int id_seguro;
    private byte grado_Dolencia;
    private String doctor_Habitual;
    private String urgencia_Medica;

    public Paciente(){

    }

    public Paciente(int id_seguro,String nombre, String doctor_Habitual) {
        this.nombre = nombre;
        this.id_seguro = id_seguro;
        this.doctor_Habitual = doctor_Habitual;
    }


    public String getNombre() {
        return nombre;
    }

    public int getId_seguro() {
        return id_seguro;
    }

    public byte getGrado_Dolencia() {
        return grado_Dolencia;
    }

    public String getDoctor_Habitual() {
        return doctor_Habitual;
    }

    public void setGrado_Dolencia(byte grado_Dolencia) {
        this.grado_Dolencia = grado_Dolencia;
    }

    public void setUrgencia(String urgencia) {
        this.urgencia_Medica = urgencia;
    }

    public String getUrgencia_Medica() {
        return urgencia_Medica;
    }
    
    

    @Override
    public int compareTo(Paciente other) {
        if(this.id_seguro < other.id_seguro)
            return -1;
        else
            if(this.id_seguro > other.id_seguro)
                return 1;
        return 0;
    }
    
    
}