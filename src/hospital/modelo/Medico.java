package hospital.modelo;

/**
 * 
 */
public class Medico implements Comparable<Medico>{
    
    private String nombre;
    private boolean estaOcupado;
    private int pacientes_Atendidos;

    public Medico(String nombre) {
        this.nombre = nombre;
        this.estaOcupado = false;
        this.pacientes_Atendidos = 0;
    }    

    public Medico(String nombre, int pacientes_Atendidos) {
        this.nombre = nombre;
        this.pacientes_Atendidos = pacientes_Atendidos;
        this.estaOcupado = false;
    }
    
    public String getNombre() {
        return nombre;
    }

    public boolean getEstaOcupado() {
        return estaOcupado;
    }

    public int getPacientes_Atendidos() {
        return pacientes_Atendidos;
    }


    public void setEstaOcupado(boolean estaOcupado) {
        this.estaOcupado = estaOcupado;
    }

    public void setPacientes_Atendidos(int pacientes_Atendidos) {
        this.pacientes_Atendidos = pacientes_Atendidos;
    }

    @Override
    public String toString() {
        return nombre + "             |               " +" Pacientes Atendidos: "+ pacientes_Atendidos ;
    }

    
    
    @Override
    public int compareTo(Medico other) {
        return this.nombre.compareTo(other.nombre);
    }
    
    
}