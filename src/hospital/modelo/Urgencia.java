package hospital.modelo;
import java.io.FileInputStream;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.LinkedList;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * 
 */
public class Urgencia{
    
    private PriorityQueue<Paciente> salaEspera;
    private List<Paciente> asegurados;
    private Medico [] medicosTurno;
    private List<Medico> medicos;
    private int numero_Asegurados;

    public Urgencia() {
        this.salaEspera = new PriorityQueue();
        this.asegurados = new LinkedList<>();
        this.medicosTurno = new Medico[20];
        this.medicos = new LinkedList<>();
        this.numero_Asegurados = 0;
    }
    
    public void inicializar()throws Exception{
        crearTurnos("");
    }
    
    /**
     * Se inicializa la información del
     * servicio de urgencias con la información de los médicos de guardia en el turno actual y
     * de los asegurados.
     */
    public void crearTurnos(String nombre)throws Exception{
        // TODO implement here
        nombre = nombre.toUpperCase();
        if(this.asegurados.isEmpty() && this.medicos.isEmpty()){
            this.leerExcel(1,"src/hospital/data/Pacientes.xls");
            this.medicos = this.leerExcel(2, "src/hospital/data/Medicos.xls");
        }else{
            if(!existeTurno(nombre)){
                for (Medico medico : this.medicos) {
                    if (medico.getNombre().equals(nombre)) {
                        for (int i = 0; i < this.medicosTurno.length; i++) {
                            if (this.medicosTurno[i] == null) {
                                this.medicosTurno[i] = medico;
                                return;
                            }
                        }
                    }
                }
            }else{
                throw new Exception("El medico "+nombre+" ya se encuentra en turno.");
            }
        }
        
        
    }
    
    private boolean existeTurno(String nombre){
        for (int i = 0; i < this.medicosTurno.length; i++) {
            if(this.medicosTurno[i]!= null){
                if(this.medicosTurno[i].getNombre().equals(nombre))
                    return true;
            }
        }
        return false;
    }

    /**
     * Dado el nombre del
     * paciente, se comprueba si está asegurado, se le asigna el grado de enfermedad y se pasa
     * a la sala de espera.
     */
    public Paciente addPaciente(String nombre,String urgencia, byte grado)throws Exception {
        // TODO implement here
        nombre = nombre.toUpperCase();
        if(buscarPaciente(nombre)==null){
            Paciente paciente = buscarAsegurado(nombre, 0);
            if (paciente != null) {
                paciente.setGrado_Dolencia(grado);
                paciente.setUrgencia(urgencia);
                this.salaEspera.add(paciente);
                return paciente;
            } else {
                throw new Exception("La persona con el nombre " + nombre + " no se encuentra registrado en el sistema.");
            }
        }else{
            throw new Exception("El paciente "+nombre+" ya esta en la sala de espera.");
        }
    }
    
    /**
     * Busca pacientes que ya se encuentran en la sala de espera.
     */
    private Paciente buscarPaciente(String nombre){
        for (Paciente paciente : this.salaEspera) {
            if(paciente != null){
                if(paciente.getNombre().equals(nombre)){
                    return paciente;
                }
            }
        }
        return null;
    }

    /**
     * Busca si el paciente se encuentra asegurado en el sistema.
     */
    public Paciente buscarAsegurado(String nombre, int numero) {
        // TODO implement here
        nombre = nombre.toUpperCase();
        if(nombre.equals("")){
            for (Paciente asegurado : this.asegurados) {
                if (asegurado.getId_seguro() == numero)
                    return asegurado;
            }
        }else{
            for (Paciente asegurado : this.asegurados) {
                if (asegurado.getNombre().equals(nombre)) 
                    return asegurado;
            }
        }
        return null;
    }
    
    public List<Medico> medicosDisponibles(){
        List<Medico> medicos = new LinkedList<>();
        for (Medico x : this.medicosTurno) {
            if(x != null){
                if(!x.getEstaOcupado())
                    medicos.add(x);
            }
        }
        return medicos;
    }

    /**
     * Se toma al paciente más grave de la sala de espera.
     */
    public Paciente atenderPaciente(String nombre)throws Exception {
        nombre = nombre.toUpperCase();
        if(hayMedicosDisponibles()){
            if (!this.salaEspera.isEmpty()) {  
                Paciente paciente = this.salaEspera.peek();
                if (!nombre.equals("")) {
                    Medico medico = estaEnTurno_Disponible(nombre);
                    if (medico != null) {
                        this.salaEspera.remove();
                        medico.setPacientes_Atendidos(medico.getPacientes_Atendidos() + 1);
                        medico.setEstaOcupado(true);
                        return paciente;
                    }
                } else {
                    Medico medico = estaEnTurno_Disponible(paciente.getDoctor_Habitual());
                    if (medico != null) {
                        this.salaEspera.remove();
                        medico.setPacientes_Atendidos(medico.getPacientes_Atendidos() + 1);
                        medico.setEstaOcupado(true);
                        return paciente;
                    }else{
                        throw new Exception("El medico habitual no se encuentra disponible, seleccione un medico por favor.");
                    }
                }
            } else {
                throw new Exception("No hay pacientes en espera.");
            }
        }else{
            throw new Exception("Los medicos en turno se encuentran ocupados.");
        }
        return null;
    }
    
    private boolean hayMedicosDisponibles(){
        for (Medico medico : this.medicosTurno) {
            if(medico != null){
                if(medico.getEstaOcupado()== false){
                    return true;
                }
            }
        }
        return false;
    }
    
    public Medico estaEnTurno_Disponible(String nombre){
        nombre = nombre.toUpperCase();
        for (Medico medico : this.medicosTurno) {
            if(medico != null){
                if(nombre.equals(medico.getNombre())&& medico.getEstaOcupado()==false)
                    return medico;
            }
        }
        return null;
    }

    /**
     * Obtiene un listado de los médicos del turno.
     */
    public LinkedList<Medico> listarTurno() {
        // TODO implement here
        LinkedList<Medico> aux = new LinkedList<>();
        for (Medico medico : this.medicosTurno) {
            if(medico != null){
                aux.add(medico);
            }
        }
        Collections.sort(aux);
        return aux;
    }
    
    
    //Lee el archivo excel y lo almacena en una matriz(sopa). El archivo solo debe ser de tipo(xls).
    public List<Medico> leerExcel(int num,String FileName) throws Exception {
        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream(FileName));
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
        int canFilas = hoja.getLastRowNum() + 1;
        int cantCol = 0;
        switch(num){
            case 1:
                ////////////////
                String nombrePaciente = "",
                doc_Habitual = "";
                int id_seguro = 0;
                for (int i = 1; i < canFilas; i++) {
                    HSSFRow filas = hoja.getRow(i);
                    cantCol = filas.getLastCellNum();
                    for (int j = 0; j < cantCol; j++) {
                        switch (j) {
                            case 0:nombrePaciente = filas.getCell(j).getStringCellValue();break;
                            case 1:id_seguro = (int)filas.getCell(j).getNumericCellValue();break;
                            case 2:doc_Habitual = filas.getCell(j).getStringCellValue();break;
                        }
                        if(j == 2){
                            Paciente paciente = new Paciente(id_seguro, nombrePaciente.toUpperCase(), doc_Habitual.toUpperCase());
                            this.asegurados.add(paciente);
                            this.numero_Asegurados++;
                        }
                    }
                    
                }
                Collections.sort(this.asegurados);
                archivoExcel.close();
                return null;
            case 2:
                ////////////////
                String nombreDoctor = "";
                boolean enTurno = false;
                boolean estaDisponible = false;
                int numPacientesAtendidos = 0;
                List<Medico> aux = new LinkedList<>();
                for (int i = 1; i < canFilas; i++) {
                    HSSFRow filas = hoja.getRow(i);
                    cantCol = filas.getLastCellNum();
                    for (int j = 0; j < cantCol; j++) {
                        switch (j) {
                            case 0:nombreDoctor = filas.getCell(j).getStringCellValue();break;
                            case 1:numPacientesAtendidos = (int)filas.getCell(j).getNumericCellValue();break;
                        }
                        if(j ==  1){
                            Medico medico = new Medico(nombreDoctor.toUpperCase(), numPacientesAtendidos);
                            aux.add(medico);
                        }
                    }
                }
                Collections.sort(aux);
                archivoExcel.close();
                return aux;
        }
        return null;
    }

    ////////////////////////////////////////////////////////////////////////////
    /**
     * GETTERS
     */
    
    public PriorityQueue<Paciente> getSalaEspera() {
        return salaEspera;
    }

    public List<Paciente> getAsegurados() {
        return asegurados;
    }

    public Medico[] getMedicosTurno() {
        return medicosTurno;
    }
    
    public List<Medico> getMedicos(){
        return medicos;
    }
    
}