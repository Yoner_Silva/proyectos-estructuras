/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital.controller;

import hospital.modelo.Paciente;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author jhone
 */
public class BuscarAseguradoController implements Initializable {
    
    private PrincipalController principal;
    
    @FXML
    private Button buscarAsegurado;
    @FXML
    private TextField numeroAsegurado;
    @FXML
    private TableView<Paciente> tableView;
    @FXML
    private TableColumn<Paciente, Integer> colSeguro;
    @FXML
    private TableColumn<Paciente, String> colNombre;
    @FXML
    private TableColumn<Paciente, String> colDoctor_Habitual;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.tableView.setSelectionModel(null);
        this.colSeguro.setCellValueFactory(new PropertyValueFactory<Paciente, Integer>("id_seguro"));
        this.colNombre.setCellValueFactory(new PropertyValueFactory<Paciente, String>("nombre"));
        this.colDoctor_Habitual.setCellValueFactory(new PropertyValueFactory<Paciente, String>("doctor_Habitual"));
        this.tableView.setItems(crearCombo(principal.urgencia.getAsegurados()));
    }    
    
    private ObservableList crearCombo(List<Paciente> x){
        ObservableList<Paciente> aux = FXCollections.observableArrayList(x);
        return aux;
    }

    @FXML
    private void handleBuscarAction(ActionEvent event) {
        try{
            String numero = this.numeroAsegurado.getText();
            if (numero.equals("")) {
                principal.crearDialogo("¡Error!", "Las casillas no pueden estar vacias.");
            } else {
                Paciente paciente = principal.urgencia.buscarAsegurado("", Integer.parseInt(numero));
                if (paciente != null) {
                    ObservableList<Paciente> aux = FXCollections.observableArrayList(paciente);
                    this.tableView.setItems(aux);
                    principal.crearDialogo("Buscar Paciente", "Busqueda exitosa.");
                } else {
                    principal.crearDialogo("Buscar Paciente", "No se encontró el paciente con el ID: " + numero);
                }
            }
        }catch(Exception e){
            principal.crearDialogo("¡Error Buscar Paciente!", e.getMessage());
        }
    }
    
    @FXML
    public void recibirParametro(PrincipalController x){
        this.principal = x;
    }
    
}
