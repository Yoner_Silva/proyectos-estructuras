/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital.controller;

import hospital.modelo.Medico;
import hospital.modelo.Paciente;
import hospital.modelo.Urgencia;
import java.net.URL;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author jhone
 */
public class PrincipalController extends Application implements Initializable {
    
    protected static Urgencia urgencia = new Urgencia();
    
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage stage) {
        try{      
            Parent root = FXMLLoader.load(getClass().getResource("/hospital/vista/PrincipalUrgencias.fxml"));
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.setTitle("URGENCIAS HOSPITALARIAS");
            stage.show();
        }catch(Exception e){
            System.err.println(e.getMessage());
        }
    }

    @FXML
    private TableView<Paciente> tableView;
    @FXML
    private TableColumn<Paciente, Integer> colSeguro;
    @FXML
    private TableColumn<Paciente, String> colNombre_Paciente;
    @FXML
    private TableColumn<Paciente, String> colUrgencia;
    @FXML
    private TableColumn<Paciente, Byte> colGrado;
    @FXML
    private TitledPane principalTurno2;
    @FXML
    private ComboBox<String> comboNombre;
    @FXML
    private Button atenderPaciente;
    @FXML
    private Button agregarPaciente;
    @FXML
    private Button buscarPaciente;
    @FXML
    private Button listarTurnos;
    @FXML
    private ComboBox<String> comboListaMedicos;
    @FXML
    private Button crearTurnos;
    @FXML
    private TableView<Medico> tableView2;
    @FXML
    private TableColumn<Medico, String> colNombre_Medico;
    @FXML
    private TableColumn<Medico, Integer> colPacientes_Atendidos;
    
    ObservableList<Paciente> listaTable;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {   
        try{
            this.urgencia.inicializar();
            this.listarTurnos.setDisable(false);
            this.buscarPaciente.setDisable(false);
            this.atenderPaciente.setDisable(false);
            this.agregarPaciente.setDisable(false);
            this.tableView.setSelectionModel(null);
            this.colSeguro.setCellValueFactory(new PropertyValueFactory<Paciente, Integer>("id_seguro"));
            this.colNombre_Paciente.setCellValueFactory(new PropertyValueFactory<Paciente, String>("nombre"));
            this.colUrgencia.setCellValueFactory(new PropertyValueFactory<Paciente, String>("urgencia_Medica"));
            this.colGrado.setCellValueFactory(new PropertyValueFactory<Paciente, Byte>("grado_Dolencia"));
            
            this.colNombre_Medico.setCellValueFactory(new PropertyValueFactory<Medico, String>("nombre"));
            this.colPacientes_Atendidos.setCellValueFactory(new PropertyValueFactory<Medico, Integer>("pacientes_Atendidos"));
            
            ObservableList<String> aux = FXCollections.observableArrayList("");
            this.comboNombre.setItems(aux);
            this.comboListaMedicos.setItems(crearCombo(urgencia.getMedicos()));
            this.listaTable = FXCollections.observableArrayList();
        }catch(Exception e){
            System.err.println("Initialize: "+e.getMessage());
        }
    }    
    
    public void crearDialogo(String title,String context){
        Alert dialogo;
        dialogo = new Alert(Alert.AlertType.INFORMATION);
        dialogo.setTitle(title);
        dialogo.setContentText(context);
        dialogo.initStyle(StageStyle.UTILITY);
        java.awt.Toolkit.getDefaultToolkit().beep();
        dialogo.showAndWait();
    }
    
    private ObservableList crearCombo(List<Medico> x){
        List<String> lista = new LinkedList<>();
        for (Medico medico : x) {
            lista.add(medico.getNombre());
        }
        ObservableList<String> aux = FXCollections.observableArrayList(lista);
        return aux;
    }

    @FXML
    private void handleAgregarAction(ActionEvent event) {
        try{      
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/hospital/vista/InsertarPaciente.fxml"));
            Parent root = (Parent)loader.load();
            InsertarPacienteController controlador = (InsertarPacienteController)loader.getController();
            
            controlador.recibirParametros(this);
            
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.setTitle("AGREGAR PACIENTE");
            stage.show();
        }catch(Exception e){
            crearDialogo("¡Error Agregar Paciente!", e.getMessage());
        }
    }
    
    public void recibirParametro(Paciente paciente){
        if(this.listaTable.isEmpty()){
            this.listaTable = FXCollections.observableArrayList(paciente);
            this.tableView.setItems(listaTable);
        }else{
            this.listaTable.add(paciente);  
            this.tableView.setItems(listaTable);
        }
    }

    @FXML
    private void handleBuscarAction(ActionEvent event) {
        try{      
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/hospital/vista/BuscarAsegurado.fxml"));
            Parent root = loader.load();
            
            BuscarAseguradoController controlador = loader.getController();
            controlador.recibirParametro(this);
            
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.setTitle("BUSCAR PACIENTE");
            stage.show();
        }catch(Exception e){
            crearDialogo("¡Error Buscar Paciente!", e.getMessage());
        }
    }

    @FXML
    private void handleListarTurnos(ActionEvent event) {
        try{
            LinkedList<Medico> aux = urgencia.listarTurno();
            ObservableList<Medico> list = FXCollections.observableArrayList(aux);
            this.tableView2.setItems(list);
        }catch(Exception e){
            crearDialogo("¡Error Listar Turnos!", e.getMessage());
        }
    }
        

    @FXML
    private void handleCrearTurnos(ActionEvent event) {
        try{      
            String nombre = this.comboListaMedicos.getValue();
            if(nombre.equals("")){
                crearDialogo("¡Error!", "Seleccione un medico, por favor.");
            }else{
                this.urgencia.crearTurnos(nombre);
                this.comboListaMedicos.getItems().remove(nombre);
                this.comboNombre.setItems(crearCombo(urgencia.medicosDisponibles()));
                this.comboNombre.setDisable(false);
                if(!this.tableView2.getItems().isEmpty()){
                    this.tableView2.getItems().add(this.urgencia.estaEnTurno_Disponible(nombre));
                    Collections.sort(this.tableView2.getItems());
                }
            }
        }catch(Exception e){
            crearDialogo("¡Error Crear Turnos!", e.getMessage());
        }
    }

    @FXML
    private void handleAtenderAction(ActionEvent event) {
        try{
            String nombre = this.comboNombre.getValue();
            Paciente paciente;
            if(nombre != null) {
                paciente = urgencia.atenderPaciente(nombre);
                this.comboNombre.getItems().remove(nombre);
            }else{
                paciente = urgencia.atenderPaciente("");
                this.comboNombre.getItems().remove(paciente.getDoctor_Habitual());
            }
            this.tableView.getItems().remove(paciente);
            this.tableView.refresh();
            this.tableView2.refresh();
        }catch(Exception e){
            crearDialogo("¡Error Atender Paciente!", e.getMessage());
        }
    }
}
