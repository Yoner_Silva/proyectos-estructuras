/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital.controller;

import hospital.modelo.Paciente;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author jhone
 */
public class InsertarPacienteController implements Initializable {
    
    private PrincipalController principal2;
    

    @FXML
    private TextField urgencia;
    @FXML
    private TextField insertarNombre;
    @FXML
    private ComboBox<String> comboGrado;
    @FXML
    private Button agregar;
    @FXML
    private Button cancelar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.comboGrado.setItems(crearCombo());
    }    
    
    private ObservableList crearCombo(){
        ObservableList<Integer> aux = FXCollections.observableArrayList(1,2,3,4,5,6,7,8,9,10);
        return aux;
    }

    @FXML
    private void handleAgregarAction(ActionEvent event) {
        try{
            String nombre = this.insertarNombre.getText();
            String urgencia = this.urgencia.getText();
            String grado = String.valueOf(this.comboGrado.getValue());

            if (nombre.equals("") || urgencia.equals("") || grado.equals("")) {
                principal2.crearDialogo("¡Error!", "Las casillas no pueden estar vacias.");
            } else {
                Paciente paciente = principal2.urgencia.addPaciente(nombre, urgencia, Byte.parseByte(grado));
                principal2.recibirParametro(paciente);
                Stage stage = (Stage)this.agregar.getScene().getWindow();
                stage.close();
            }
        }catch(Exception e){
            principal2.crearDialogo("¡Error Agregar Paciente!", e.getMessage());
        }
    }
    

    @FXML
    private void handleCancelarAction(ActionEvent event) {
        Stage stage = (Stage)this.cancelar.getScene().getWindow();
        stage.close();
    }
    
    public void recibirParametros(PrincipalController x){
        this.principal2 = x;
    }
    
}
