/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueadero;

/**
 *
 * @author Yoner silva
 */
public class Reparacion {
    private String descripcion;
    private int kms;

    public Reparacion(String descripcion, int kms) {
        this.descripcion = descripcion;
        this.kms = kms;
    }
    
    public String ToString(){
        return "Descripción: "+descripcion+"              "+"Kilometros: "+kms +"\n\n";
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getKms() {
        return kms;
    }

    public void setKms(int kms) {
        this.kms = kms;
    }
    
}
