/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueadero;

import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author Yoner Silva
 */
public class Garaje {
    LinkedList<Coche> coches;

    public Garaje() {
        coches = new LinkedList();
        
    }
    
    public void addCoche(String direccionDueno, String matricula)throws Exception{
        if(coches != null){
            Iterator <Coche> it = coches.iterator();
            while(it.hasNext()){
                Coche x = it.next();
                if(x.getMatricula().equals(matricula))
                    throw new Exception("Ya se encuentra registrado un coche con la matricula "+matricula);
            }
            coches.add(new Coche(direccionDueno, matricula));
        }
        else{
            coches.add(new Coche(direccionDueno, matricula));
        }
    }
    
    public Coche buscarCoche(String matricula){
        Iterator <Coche> it = coches.iterator();
            while(it.hasNext()){
                Coche x = it.next();
                if(x.getMatricula().equals(matricula))
                    return x;
            }
         return null;
         
    }

    public LinkedList<Coche> getCoches() {
        return coches;
    }
    
    
}
