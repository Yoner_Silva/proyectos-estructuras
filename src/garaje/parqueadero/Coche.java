/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueadero;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Yoner Silva
 */
public class Coche {
    private String direccionDueno;
    private String matricula;
    LinkedList<Reparacion> reparaciones;

    public Coche(String direccionDueno, String matricula) {
        this.direccionDueno = direccionDueno;
        this.matricula = matricula;
        reparaciones = new LinkedList();        
    }

    public String getDireccionDueno() {
        return direccionDueno;
    }

    public void setDireccionDueno(String direccionDueno) {
        this.direccionDueno = direccionDueno;
    }

    public String getMatricula() {
        return matricula;
    }

    
    public void addReparacion(String descripcion, int kms)throws Exception{
        ///deben tener en cuenta que la reparación que se va agregar los kilometros no se han inferior a la reparación anterior
        
        if(reparaciones.size()!=0){
            Reparacion ultima = buscarUltimaReparación();
            if(kms > ultima.getKms())
                reparaciones.add(new Reparacion(descripcion,kms));
            else{
                throw new Exception("Los Km actuales son menores a los Km de la última reparación.");
            }
        }else{
            reparaciones.add(new Reparacion(descripcion,kms));
        }
    }
    
    public LinkedList buscarReparaciones(String palabraClave){
        LinkedList<Reparacion> reparacionesEspecificas = new LinkedList();
        if(reparaciones.size() != 0){
            Iterator <Reparacion> it = reparaciones.iterator();
            while(it.hasNext()){
                Reparacion x = it.next();
                if(x.getDescripcion().contains(palabraClave))
                    reparacionesEspecificas.add(x);
            }
        }
        return reparacionesEspecificas;
        
    }
    
    public Reparacion buscarUltimaReparación(){
        return reparaciones.getLast();
    } 
    
    public String ToString(){
        return "Matricula: "+matricula + "\n"+"Dirección del dueño: "+direccionDueno;
    }

    public List<Reparacion> getReparaciones() {
        return reparaciones;
    }
    
    
}
