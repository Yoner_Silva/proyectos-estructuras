package aerolinea;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @Yoner Silva
 * @Leiddy Vacca
 */
public class Vuelo implements Comparable<Vuelo>{
    
    private Silla [][] sillas;
    private List<Reserva> reservas;
    private double capacidad;
    private int sillasDesocupadas;
    private int codigo;
    private Fecha date;
    private Ciudad destino;
    private final Ciudad origen = new Ciudad("BOGOTA", 0, 0);;

    public Vuelo() {
        
    }
    
    public Vuelo(int nada){
        this.sillas = new Silla[15][5];
        crearSillas();
    }
    
    public Vuelo(int codigo, Fecha date, Ciudad destino) {
        this.codigo = codigo;
        this.date = date;
        this.destino = destino;
        this.reservas = new ArrayList<>(5);
        this.sillas = new Silla[15][5];
        crearSillas();
        this.capacidad = 100.0;
        this.sillasDesocupadas = 75;
        
    }
    
    public boolean reservarVuelo(String nombre, int cedula, String silla){
        Silla x = validarCrearReserva(silla);
        if(x != null){
            Reserva reserva = buscarReserva(cedula);
            if(reserva != null) {
                x.setEstaOcupada(true);
                reserva.getPuestos().add(x);
                this.sillasDesocupadas--;
                actualizarCapacidad();
                return true;
            } else {
                Reserva aux = new Reserva(this.codigo,nombre, cedula);
                x.setEstaOcupada(true);
                aux.getPuestos().add(x);
                reservas.add(aux);
                this.sillasDesocupadas--;
                actualizarCapacidad();
                return true;
            }
        }
        return false;
    }
    
    public void cancelarReserva(List<Silla> x){
        boolean seEncontró;
        while(x.size() != 0){
            Silla aux = x.get(0);
            seEncontró = false;
            for (int i = 0;(i < this.sillas.length) && (seEncontró != true); i++) {
                for (int j = 0;(j < this.sillas[i].length) && (seEncontró != true); j++) {
                    if(this.sillas[i][j].equals(aux)){
                        this.sillas[i][j].setEstaOcupada(false);
                        this.sillasDesocupadas++;
                        seEncontró = true;
                        x.remove(aux);
                    }
                }
            }
        }
        actualizarCapacidad();
    }
    
    private Reserva buscarReserva(int cedula){
        for (Reserva aux : reservas) {
            if(aux.getCedula()==cedula)
                return aux;
        }
        return null;
    }
    
    private Silla validarCrearReserva(String silla){
        Silla puesto = new Silla(silla);
        for (int i = 0; i < this.sillas.length; i++) {
            for (int j = 0; j < this.sillas[i].length; j++) {
                Silla x = this.sillas[i][j];
                if(x.getSilla().equals(puesto.getSilla())){
                    if(x.isEstaOcupada())
                        return null;
                    else
                        return x;
                }
            }
        }
        return null;
    }
    
    private void crearSillas(){
        String cadena = "";
        for (int i = 0; i < this.sillas.length; i++) {
            for (int j = 0; j < this.sillas[i].length; j++) {
                cadena = String.valueOf(i)+letraSilla(j);
                this.sillas[i][j] = new Silla(cadena);
            }
        }
        
    }
    
    private String letraSilla(int j){
        switch(j){
            case(0):
                return "-A";
            case(1):
                return "-B";
            case(2):
                return "-C";
            case(3):
                return "-D";
            case(4):
                return "-E";
        }
        return null;
    }
    
    public void actualizarCapacidad(){
        int totalSillas = 75;
        this.capacidad = (this.sillasDesocupadas*100)/totalSillas;
    }

    ////////////////////////////////////////////////////////////////////////////
    //GETTERS & SETTERS
    ////////////////////////////////////////////////////////////////////////////
    
    public Silla[][] getSillas() {
        return sillas;
    }

    public void setSillas(Silla[][] sillas) {
        this.sillas = sillas;
    }

    public int getSillasDesocupadas() {
        return sillasDesocupadas;
    }

    public void setSillasDesocupadas(int sillasDesocupadas) {
        this.sillasDesocupadas = sillasDesocupadas;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Fecha getDate() {
        return date;
    }

    public void setDate(Fecha date) {
        this.date = date;
    }

    public Ciudad getDestino() {
        return destino;
    }

    public List<Reserva> getReservas() {
        return reservas;
    }

    public double getCapacidad() {
        return capacidad;
    }

    public Ciudad getOrigen() {
        return origen;
    }
    
    

    
    
    @Override
    public int compareTo(Vuelo other) {
        return this.getDate().compareTo(other.getDate());
    }
    
    
}
