package aerolinea;

/**
 *
 * @Yoner Silva
 * @Leiddy Vacca
 */
public class Ciudad implements Comparable<Ciudad>{
    
    private String ciudad;
    private double coordenada_X, coordenada_Y;

    public Ciudad(){
        
    }

    public Ciudad(String ciudad, double coordenada_X, double coordenada_Y) {
        this.ciudad = ciudad;
        this.coordenada_X = coordenada_X;
        this.coordenada_Y = coordenada_Y;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    //GETTERS & SETTERS
    ////////////////////////////////////////////////////////////////////////////
    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public double getCoordenada_X() {
        return coordenada_X;
    }

    public void setCoordenada_X(double coordenada_X) {
        this.coordenada_X = coordenada_X;
    }

    public double getCoordenada_Y() {
        return coordenada_Y;
    }

    public void setCoordenada_Y(double coordenada_Y) {
        this.coordenada_Y = coordenada_Y;
    }

    @Override
    public int compareTo(Ciudad other) {
        if(     
                (this.getCiudad().equals(other.getCiudad()))
                &&(this.coordenada_X == other.getCoordenada_X())
                &&(this.coordenada_Y == other.getCoordenada_Y())
          )
            return 0;
        else
            if(
                    (other.getCiudad().compareTo(this.getCiudad())==1)
                    &&(other.getCoordenada_X() > this.getCoordenada_X())
                    &&(other.getCoordenada_Y() > this.getCoordenada_Y())
              )
                return 1;
            else
                return -1;
    }

    @Override
    public String toString() {
        return this.getCiudad();
    }
    
    
    
}
