/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aerolinea.controller;

import aerolinea.Ciudad;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;

/**
 *
 * @Yoner Silva
 * @Leiddy Vacca
 */
public class EliminarCiudadController extends PrincipalController implements Initializable{

    @FXML
    private ComboBox<String> ciudad;
    @FXML
    private Button buttonAceptar;
    @FXML
    private Button buttonCancelar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.ciudad.setItems(darValor());
    }
    
    private ObservableList<String> darValor(){
        List<String> aux = new LinkedList<>();
        for(Ciudad ciudad: super.getAero().getCiudades()){
            aux.add(ciudad.getCiudad());
        }
        ObservableList<String> options = FXCollections.observableList(aux);
        return options;
    }
    
    @FXML
    private void handleAceptarAction(ActionEvent event){
        try{
            String ciudad = this.ciudad.getValue();
            if (ciudad.equals("")) {
                super.crearDialogo("¡Error!","Las casillas no pueden estar vacías.");
            } else {
                boolean removeCiudad = super.getAero().removeCiudad(ciudad);
                if (removeCiudad) {
                    super.crearDialogo("Eliminar Ciudad","La ciudad "+ciudad+" fue añadida con exito.");
                    Stage stage = (Stage) this.buttonAceptar.getScene().getWindow();
                    stage.close();
                }
            }
        }catch(Exception e){
            System.err.println("Error al eliminar la ciudad. "+e.getMessage());
        }
    }

    @FXML
    private void handleCancelarAction(ActionEvent event) {
        Stage stage = (Stage) this.buttonCancelar.getScene().getWindow();
        stage.close();
    }
    
}
