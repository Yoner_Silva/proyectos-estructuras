/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aerolinea.controller;

import aerolinea.Silla;
import aerolinea.Vuelo;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

/**
 *
 * @Yoner Silva
 * @Leiddy Vacca
 */
public class Opcion2Controller extends PrincipalController implements Initializable{

    @FXML
    private TextField buscarCodigo;
    @FXML
    private TextField codigo;
    @FXML
    private TextField fecha;
    @FXML
    private TextField porcentaje;
    @FXML
    private Button buscarVuelo;
    
    
    @FXML
    private CheckBox A0,B0,C0,D0,E0;
    @FXML
    private CheckBox A1,B1,C1,D1,E1;
    @FXML
    private CheckBox A2,B2,C2,D2,E2;
    @FXML
    private CheckBox A3,B3,C3,D3,E3;
    @FXML
    private CheckBox A4,B4,C4,D4,E4;
    @FXML
    private CheckBox A5,B5,C5,D5,E5;
    @FXML
    private CheckBox A6,B6,C6,D6,E6;
    @FXML
    private CheckBox A7,B7,C7,D7,E7;
    @FXML
    private CheckBox A8,B8,C8,D8,E8;
    @FXML
    private CheckBox A9,B9,C9,D9,E9;
    @FXML
    private CheckBox A10,B10,C10,D10,E10;
    @FXML
    private CheckBox A11,B11,C11,D11,E11;
    @FXML
    private CheckBox A12,B12,C12,D12,E12;
    @FXML
    private CheckBox A13,B13,C13,D13,E13;
    @FXML
    private CheckBox A14,B14,C14,D14,E14;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }
    
    @FXML
    private void handleBuscarVueloAction(ActionEvent event) {
        try{
            limpiar();
            String buscarCodigo = this.buscarCodigo.getText();
            if(buscarCodigo.equals("")){
                super.crearDialogo("Opcion 2","Las casillas no pueden estar vacías.");
            }else{
                Vuelo vuelo = super.getAero().buscarVuelo(Integer.parseInt(buscarCodigo));
                if(vuelo != null){
                    this.codigo.setText(buscarCodigo);
                    this.fecha.setText(vuelo.getDate().toString());
                    this.porcentaje.setText(vuelo.getCapacidad()+"%");
                    Silla silla;
                    for (int i = 0; i < vuelo.getSillas().length; i++) {
                        for (int j = 0; j < vuelo.getSillas()[i].length; j++) {
                            silla = vuelo.getSillas()[i][j];
                            
                            if(silla.isEstaOcupada()){
                                modificarCheckBox(silla.getSilla());
                            }
                        }
                    }
                    super.crearDialogo("Opcion 2","El vuelo con el codigo "+buscarCodigo+" fue encontrado con éxito.");
                }else{
                    super.crearDialogo("Opcion 2","El vuelo con el codigo "+buscarCodigo+ " no se encontró."+ '\n' + "Intente nuevamente. :(");
                }
            }
        }catch(Exception e){
            System.err.println("Error en buscar Vuelo: Opción 2 - "+e.getMessage());
        }
    }
    
    private void limpiar(){
        this.codigo.setText("");
        this.fecha.setText("");
        this.porcentaje.setText("");
        
        A0.setSelected(false); B0.setSelected(false); C0.setSelected(false); D0.setSelected(false); E0.setSelected(false);
        A1.setSelected(false); B1.setSelected(false); C1.setSelected(false); D1.setSelected(false); E1.setSelected(false);
        A2.setSelected(false); B2.setSelected(false); C2.setSelected(false); D2.setSelected(false); E2.setSelected(false);
        A3.setSelected(false); B3.setSelected(false); C3.setSelected(false); D3.setSelected(false); E3.setSelected(false);
        A4.setSelected(false); B4.setSelected(false); C4.setSelected(false); D4.setSelected(false); E4.setSelected(false);
        A5.setSelected(false); B5.setSelected(false); C5.setSelected(false); D5.setSelected(false); E5.setSelected(false);
        A6.setSelected(false); B6.setSelected(false); C6.setSelected(false); D6.setSelected(false); E6.setSelected(false);
        A7.setSelected(false); B7.setSelected(false); C7.setSelected(false); D7.setSelected(false); E7.setSelected(false);
        A8.setSelected(false); B8.setSelected(false); C8.setSelected(false); D8.setSelected(false); E8.setSelected(false);
        A9.setSelected(false); B9.setSelected(false); C9.setSelected(false); D9.setSelected(false); E9.setSelected(false);
        A10.setSelected(false); B10.setSelected(false); C10.setSelected(false); D10.setSelected(false); E10.setSelected(false);
        A11.setSelected(false); B11.setSelected(false); C11.setSelected(false); D11.setSelected(false); E11.setSelected(false);
        A12.setSelected(false); B12.setSelected(false); C12.setSelected(false); D12.setSelected(false); E12.setSelected(false);
        A13.setSelected(false); B13.setSelected(false); C13.setSelected(false); D13.setSelected(false); E13.setSelected(false);
        A14.setSelected(false); B14.setSelected(false); C14.setSelected(false); D14.setSelected(false); E14.setSelected(false);
        
    }
    
    private void modificarCheckBox(String silla){
        switch(silla){
        case "0-A": A0.setSelected(true); break; case "0-B": B0.setSelected(true); break; case "0-C": C0.setSelected(true); break; case "0-D": D0.setSelected(true); break; case "0-E": E0.setSelected(true); break;
        case "1-A": A1.setSelected(true); break; case "1-B": B1.setSelected(true); break; case "1-C": C1.setSelected(true); break; case "1-D": D1.setSelected(true); break; case "1-E": E1.setSelected(true); break;
        case "2-A": A2.setSelected(true); break; case "2-B": B2.setSelected(true); break; case "2-C": C2.setSelected(true); break; case "2-D": D2.setSelected(true); break; case "2-E": E2.setSelected(true); break;
        case "3-A": A3.setSelected(true); break; case "3-B": B3.setSelected(true); break; case "3-C": C3.setSelected(true); break; case "3-D": D3.setSelected(true); break; case "3-E": E3.setSelected(true); break;
        case "4-A": A4.setSelected(true); break; case "4-B": B4.setSelected(true); break; case "4-C": C4.setSelected(true); break; case "4-D": D4.setSelected(true); break; case "4-E": E4.setSelected(true); break;
        case "5-A": A5.setSelected(true); break; case "5-B": B5.setSelected(true); break; case "5-C": C5.setSelected(true); break; case "5-D": D5.setSelected(true); break; case "5-E": E5.setSelected(true); break;
        case "6-A": A6.setSelected(true); break; case "6-B": B6.setSelected(true); break; case "6-C": C6.setSelected(true); break; case "6-D": D6.setSelected(true); break; case "6-E": E6.setSelected(true); break;
        case "7-A": A7.setSelected(true); break; case "7-B": B7.setSelected(true); break; case "7-C": C7.setSelected(true); break; case "7-D": D7.setSelected(true); break; case "7-E": E7.setSelected(true); break;
        case "8-A": A8.setSelected(true); break; case "8-B": B8.setSelected(true); break; case "8-C": C8.setSelected(true); break; case "8-D": D8.setSelected(true); break; case "8-E": E8.setSelected(true); break;
        case "9-A": A9.setSelected(true); break; case "9-B": B9.setSelected(true); break; case "9-C": C9.setSelected(true); break; case "9-D": D9.setSelected(true); break; case "9-E": E9.setSelected(true); break;
        case "10-A": A10.setSelected(true); break; case "10-B": B10.setSelected(true); break; case "10-C": C10.setSelected(true); break; case "10-D": D10.setSelected(true); break; case "10-E": E10.setSelected(true); break;
        case "11-A": A11.setSelected(true); break; case "11-B": B11.setSelected(true); break; case "11-C": C11.setSelected(true); break; case "11-D": D11.setSelected(true); break; case "11-E": E11.setSelected(true); break;
        case "12-A": A12.setSelected(true); break; case "12-B": B12.setSelected(true); break; case "12-C": C12.setSelected(true); break; case "12-D": D12.setSelected(true); break; case "12-E": E12.setSelected(true); break;
        case "13-A": A13.setSelected(true); break; case "13-B": B13.setSelected(true); break; case "13-C": C13.setSelected(true); break; case "13-D": D13.setSelected(true); break; case "13-E": E13.setSelected(true); break;
        case "14-A": A14.setSelected(true); break; case "14-B": B14.setSelected(true); break; case "14-C": C14.setSelected(true); break; case "14-D": D14.setSelected(true); break; case "14-E": E14.setSelected(true); break;
        }
    }
    
    
}
