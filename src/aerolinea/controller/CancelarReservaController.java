/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aerolinea.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @Yoner Silva
 * @Leiddy Vacca
 */
public class CancelarReservaController extends PrincipalController implements Initializable {

    @FXML
    private TextField codigo;
    @FXML
    private Button buttonCancelar;
    @FXML
    private TextField nombre;
    @FXML
    private TextField cedula;
    @FXML
    private Button buttonAceptar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }

    @FXML
    private void handleAceptarAction(ActionEvent event){
        try{
            String codigo = this.codigo.getText();
            String nombre = this.nombre.getText();
            String cedula = this.cedula.getText();
            if ((codigo.equals("")) || (nombre.equals("")) || (cedula.equalsIgnoreCase(""))) {
                super.crearDialogo("¡Error!","Las casillas no puden estar vacias.");
            } else {
                boolean cancelarReserva = super.getAero().cancelarReserva(Integer.parseInt(codigo), nombre, Integer.parseInt(cedula));
                if (cancelarReserva) {
                    super.crearDialogo("Cancelar Reserva","La reserva fue cancelada con exito.");
                    Stage stage = (Stage) this.buttonAceptar.getScene().getWindow();
                    stage.close();
                } else {
                    super.crearDialogo("Cancelar Reserva", "La reserva no se encontró o el vuuelo no existe."+ '\n' + "Intente nuevamente. :(");                   
                }
            }
        }catch(Exception e){
            System.err.println("Error al cancelar la reserva. "+e.getMessage());
        }
    }

    @FXML
    private void handleCancelarAction(ActionEvent event) {
        Stage stage = (Stage) this.buttonCancelar.getScene().getWindow();
        stage.close();
    }
    
}
