/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aerolinea.controller;

import aerolinea.Ciudad;
import java.net.URL;
import java.time.LocalDate;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @Yoner Silva
 * @Leiddy Vacca
 */
public class AgregarVueloController extends PrincipalController implements Initializable{

    @FXML
    private DatePicker fecha;
    @FXML
    private TextField codigo;
    @FXML
    private ComboBox<String> ciudad;
    @FXML
    private Button agregarVuelo;
    @FXML
    private TextField hora;
    
    @Override
    public void initialize(URL location, ResourceBundle resources){
        this.ciudad.setItems(darValor());
    }
    
    private ObservableList<String> darValor(){
        List<String> aux = new LinkedList<>();
        for(Ciudad ciudad: super.getAero().getCiudades()){
            aux.add(ciudad.getCiudad());
        }
        ObservableList<String> options = FXCollections.observableList(aux);
        return options;
    }
    
    @FXML
    private void handleAgregarVueloAction(ActionEvent event) {
        try{
            String ciudad = this.ciudad.getValue();
            String codigo = this.codigo.getText();
            LocalDate fecha = this.fecha.getValue();
            String hora = this.hora.getText();
            if ((ciudad == null) || (codigo.equals("")) || (fecha == null) || (hora.equals(""))) {
                super.crearDialogo("¡Error!","Las casillas no pueden estar vacías.");
            } else {
                String cadena = comprobarHora(hora);
                if( cadena != null){
                    String[] x = cadena.split(":");
                    Date date = new Date(fecha.getDayOfYear(), fecha.getMonthValue(), fecha.getDayOfMonth(), Integer.parseInt(x[0]), Integer.parseInt(x[1]));
                    Ciudad c = super.getAero().buscarCiudad(ciudad);
                    boolean addVuelo = super.getAero().addVuelo(Integer.parseInt(codigo), date, c);
                    if (addVuelo) {
                        super.crearDialogo("Agregar Vuelo", "El vuelo fue añadido con exito.");
                        Stage stage = (Stage) this.agregarVuelo.getScene().getWindow();
                        stage.close();
                    } else {
                        super.crearDialogo("Agregar Vuelo", "El vuelo no se pudo agregar debido a que ya existe uno." + '\n' + "Intente nuevamente. :(");
                    }
                }else{
                    super.crearDialogo("¡Error!", "La hora esta mal. Por favor digite en formato: 'HORA:MIN' ");
                }
            }
        }catch(Exception e){
            System.err.println("Error al agregar vuelo. "+e.getMessage());
        }
    }

    private String comprobarHora(String hora){
        int h = 0;
        int min = 0;

        if(hora.contains(":")){
            String[] x = hora.split(":");
            h = Integer.parseInt(x[0]);
            min = Integer.parseInt(x[1]);
            if((h>=0 && h<=24)&&(min>=0 && min<=59))
                return hora;
        }else{
            h = Integer.parseInt(hora);
            if(h>=0 && h<=24)
                return h+":00";
        }
        
        return null;
    }
    
    
}
