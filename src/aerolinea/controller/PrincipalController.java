/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aerolinea.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

import aerolinea.Aerolinea;
import aerolinea.Ciudad;
import aerolinea.Fecha;
import aerolinea.Silla;
import aerolinea.Vuelo;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
/**
 *
 * @Yoner Silva
 * @Leiddy Vacca
 */
public class PrincipalController extends Application implements Initializable{
    
    private static Aerolinea aero = new Aerolinea();
    private static Vuelo vuelo = new Vuelo();
    
    public static void main(String[] args) {
        launch(args);
    }

    
    @Override
    public void start(Stage stage) throws Exception {
        try{      
            Parent root = FXMLLoader.load(getClass().getResource("/aerolinea/interfaz/PrincipalAerolinea.fxml"));
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.setTitle("AEROLINEA LATINCOL");
            stage.show();
        }catch(Exception e){
            System.err.println(e.getMessage());
        }
    }  
    
    @FXML
    private TableView<Vuelo> tableView;
    @FXML
    private TableColumn<Vuelo, String> colListaVuelos;
    @FXML
    private TableColumn<Vuelo, Integer> colCodigo;
    @FXML
    private TableColumn<Vuelo, Ciudad> colDestino;
    @FXML
    private TableColumn<Vuelo, Ciudad> colOrigen;
    @FXML
    private TableColumn<Vuelo, Integer> colSillasLibres;
    @FXML
    private TableColumn<Vuelo, Double> colCapacidad;
    @FXML
    private TableColumn<Vuelo, Fecha> colFecha;
    @FXML
    private TitledPane opcion1;
    @FXML
    private ComboBox<String> ciudad;
    @FXML
    private Button agregarVuelo;
    @FXML
    private Button eliminarVuelo;
    @FXML
    private Button agregarCiudad;
    @FXML
    private Button eliminarCiudad;
    @FXML
    private Button buttonBuscar;
    @FXML
    private Button buttonOpcion2;
    @FXML
    private Button buttonOpcion3;
    @FXML
    private Button cancelarReserva;
    
    @FXML
    private TextField date;
    @FXML
    private TextField codigo;
    @FXML
    private TextField porcentaje;
    @FXML
    private TextField nombres;
    @FXML
    private TextField apellidos;
    @FXML
    private TextField cedula;
    @FXML
    private Button botonMostrarVuelo;
    @FXML
    private Button buttonReservar;
    @FXML
    private Button buttonEmbarque;
    
    @FXML
    private CheckBox A0,B0,C0,D0,E0;
    @FXML
    private CheckBox A1,B1,C1,D1,E1;
    @FXML
    private CheckBox A2,B2,C2,D2,E2;
    @FXML
    private CheckBox A3,B3,C3,D3,E3;
    @FXML
    private CheckBox A4,B4,C4,D4,E4;
    @FXML
    private CheckBox A5,B5,C5,D5,E5;
    @FXML
    private CheckBox A6,B6,C6,D6,E6;
    @FXML
    private CheckBox A7,B7,C7,D7,E7;
    @FXML
    private CheckBox A8,B8,C8,D8,E8;
    @FXML
    private CheckBox A9,B9,C9,D9,E9;
    @FXML
    private CheckBox A10,B10,C10,D10,E10;
    @FXML
    private CheckBox A11,B11,C11,D11,E11;
    @FXML
    private CheckBox A12,B12,C12,D12,E12;
    @FXML
    private CheckBox A13,B13,C13,D13,E13;
    @FXML
    private CheckBox A14,B14,C14,D14,E14;
    
    private static List<String> sillas = new LinkedList<>();
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.ciudad.setItems(darValor());
        modificarColumnas();
        limpiar();
    }
    
    
    private ObservableList<String> darValor(){
        List<String> x = new LinkedList<>();
        for(Ciudad ciudad: this.aero.getCiudades()){
            x.add(ciudad.getCiudad());
        }
        ObservableList<String> options = FXCollections.observableList(x);
        return options;
    }
    
    private void modificarColumnas(){
        this.colCodigo.setCellValueFactory(new PropertyValueFactory<Vuelo, Integer>("codigo"));
        this.colDestino.setCellValueFactory(new PropertyValueFactory<Vuelo, Ciudad>("destino"));
        this.colOrigen.setCellValueFactory(new PropertyValueFactory<Vuelo, Ciudad>("origen"));
        this.colSillasLibres.setCellValueFactory(new PropertyValueFactory<Vuelo, Integer>("sillasDesocupadas"));
        this.colCapacidad.setCellValueFactory(new PropertyValueFactory<Vuelo, Double>("capacidad"));
        this.colFecha.setCellValueFactory(new PropertyValueFactory<Vuelo, Fecha>("date"));

    }
    
    @FXML
    private void handleActualizarAction(MouseEvent event){
        actualizarAction();
        this.opcion1.setExpanded(false);
    }
    
    private void actualizarAction() {
        modificarColumnas();
        ObservableList<Vuelo> aux = FXCollections.observableArrayList(this.aero.getVuelos());
        this.tableView.setItems(aux);
        this.tableView.refresh();
    }
    
    @FXML
    private void handleMostrarVueloAction(ActionEvent event){
        try{
            if (!this.aero.getVuelos().isEmpty()) {
                boolean valida = mostrarVuelo();
                if(!valida){
                    crearDialogo("¡Error!", "Seleccione un vuelo primero.");
                }
            } else {
                crearDialogo("Mostrar Vuelo", "No hay vuelos disponibles.");
            }
        }catch(Exception e){
            System.err.println("Error en mostar Vuelo. "+e.getMessage());
        }
    }
    
    private boolean mostrarVuelo(){
        final Vuelo v = getTablaVueloSeleccionado();
        if (v != null) {
            this.vuelo = v;
            this.codigo.setText(String.valueOf(this.vuelo.getCodigo()));
            this.date.setText(this.vuelo.getDate().toString());
            this.porcentaje.setText(String.valueOf(this.vuelo.getCapacidad()));
            this.nombres.setText("");
            this.apellidos.setText("");
            this.cedula.setText("");
            
            Silla silla;
            for (int i = 0; i < this.vuelo.getSillas().length; i++) {
                for (int j = 0; j < this.vuelo.getSillas()[i].length; j++) {
                    silla = this.vuelo.getSillas()[i][j];

                    if (silla.isEstaOcupada()) {
                        modificarCheckBox(silla.getSilla(),true,true);
                    }else{
                        modificarCheckBox(silla.getSilla(), false, false);
                    }
                }
            }
            return true;
        }
        return false;
        
    }
    
    private Vuelo getTablaVueloSeleccionado() {
        if (this.tableView != null) {
            List<Vuelo> tabla = this.tableView.getSelectionModel().getSelectedItems();
            if (tabla.size() == 1) {
                final Vuelo seleccion = tabla.get(0);
                return seleccion;
            }
        }
        return null;
    }
    
    private void ponerVueloSeleccionado() {
        final Vuelo v = getTablaVueloSeleccionado();
        if (v != null) {
            this.vuelo = v;
        }
    }
    
    public void crearDialogo(String title,String context){
        Alert dialogo;
        dialogo = new Alert(Alert.AlertType.INFORMATION);
        dialogo.setTitle(title);
        dialogo.setContentText(context);
        dialogo.initStyle(StageStyle.UTILITY);
        java.awt.Toolkit.getDefaultToolkit().beep();
        dialogo.showAndWait();
    }
    
    @FXML
    private void handleReservarAction(ActionEvent event) {
        try {
            String nombres = this.nombres.getText();
            String apellidos = this.apellidos.getText();
            String cedula = this.cedula.getText();
            ponerVueloSeleccionado();
            if(this.vuelo != null){
                if(nombres.equals("") || apellidos.equals("") || cedula.equals("")){
                    crearDialogo("¡Error!", "No se permiten casillas vacias.");
                }else{
                    if (!this.sillas.isEmpty()) {
                        String name = nombres + " " + apellidos;
                        for (String silla : this.sillas) {
                            this.vuelo.reservarVuelo(name, Integer.parseInt(cedula), silla);
                        }
                        this.sillas.clear();
                        mostrarVuelo();
                        this.tableView.refresh();
                        crearDialogo("Reservar", "La reserva se hizo con exito.");
                    } else {
                        crearDialogo("¡Error!", "Por favor, seleccione los puestos a reservar.");
                    }
                }
            }else{
                crearDialogo("¡Error!", "Por favor, seleccione un vuelo primero");
            }
        }catch(Exception e){
            System.err.println("Error boton Reservar. "+e.getMessage());
        }
    }
    
    @FXML
    private void handleCancelarReserva(ActionEvent event){
        try{
            Parent root = FXMLLoader.load(getClass().getResource("/aerolinea/interfaz/CancelarReserva.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.setTitle("CANCELAR RESERVA");
            stage.show();
        }catch(Exception e){
            System.err.println("Error en el botón cancelar Reserva. "+e.getMessage());
        }
    }
    
    
    @FXML
    private void handleAgregarVueloAction(ActionEvent event) {
        this.tableView.refresh();
        try{
            Parent root = FXMLLoader.load(getClass().getResource("/aerolinea/interfaz/AgregarVuelo.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.setTitle("AGREGAR VUELO");
            stage.show();
        }catch(IOException e){
            System.err.println("Error boton agregar Vuelo. "+e.getCause().getMessage());
        }
    }

    @FXML
    private void handleEliminarVueloAction(ActionEvent event) {
        try{
            ponerVueloSeleccionado();
            if(this.vuelo != null){
                boolean eliminar = this.aero.removeVuelo(this.vuelo.getCodigo());
                if(eliminar){
                    ObservableList<Vuelo> aux = FXCollections.observableList(this.aero.getVuelos());
                    if(aux.isEmpty())
                        this.tableView.refresh();
                    else
                        this.tableView.setItems(aux);
                    limpiar();
                    crearDialogo("Eliminar Vuelo", "El Vuelo "+this.vuelo.getCodigo()+" fue eliminado con exito.");
                }else{
                    crearDialogo("Eliminar Vuelo","El vuelo "+this.vuelo.getCodigo()+" no se encontró."+ '\n'+ "Intente nuevamente. :(");
                }
            }else{
                crearDialogo("¡Error!", "Seleccione un vuelo de la tabla.");
            }
        }catch(Exception e){
            System.err.println("Error boton eliminar Vuelo. "+e.getMessage());
        }
    }

    @FXML
    private void handleAgregarCiudadAction(ActionEvent event) {
        try{
            Parent root = FXMLLoader.load(getClass().getResource("/aerolinea/interfaz/AgregarCiudad.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.setTitle("AGREGAR CIUDAD");
            stage.show();
        }catch(Exception e){
            System.err.println("Error boton agregar Ciudad. "+e.getStackTrace().toString());
        }
    }
    
    @FXML
    private void handleEliminarCiudadAction(ActionEvent event) {
        try{
            Parent root = FXMLLoader.load(getClass().getResource("/aerolinea/interfaz/EliminarCiudad.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.setTitle("ELIMINAR CIUDAD");
            stage.show();
        }catch(Exception e){
            System.err.println("Error boton eliminar Ciudad. "+e.getMessage());
        }
    }
    
    @FXML
    private void handleOpcion1Action(MouseEvent event) {
        this.ciudad.setItems(darValor());
        
    }
    
    
    @FXML
    private void handleBuscarOpcion1Action(ActionEvent event) {
        try{
            String ciudad = this.ciudad.getValue();
            if(ciudad.equals("")){
                crearDialogo("¡Error!","Las casillas no pueden estar vacías.");
            }else{
                Ciudad c = this.aero.buscarCiudad(ciudad);
                List<Vuelo> aux = this.aero.opcion1(c);
                ObservableList<Vuelo> vuelos = FXCollections.observableList(aux);
                this.tableView.setItems(vuelos);
                this.opcion1.setExpanded(false);
                if(!aux.isEmpty()){
                    crearDialogo("Opcion 1","Se encontraron vuelos.");
                }else{
                    crearDialogo("Opcion 1","No se encontraron vuelos."+ '\n'+ "Intente nuevamente. :(");
                }
            }
        }catch(Exception e){
            System.err.println("Error boton opcion 1. "+e.getCause().getMessage());
        }
    }
    
    @FXML
    private void handleOpcion2Action(ActionEvent event) {
        try{
            Parent root = FXMLLoader.load(getClass().getResource("/aerolinea/interfaz/Opcion2.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.setTitle("OPCION 2");
            stage.show();
        }catch(Exception e){
            System.err.println("Error boton opcion 2. "+e.getMessage());
        }
    }
    
    @FXML
    private void handleOpcion3Action(ActionEvent event) {
        try{
            Parent root = FXMLLoader.load(getClass().getResource("/aerolinea/interfaz/Opcion3.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);
            stage.setFullScreen(false);
            stage.setTitle("OPCION 3");
            stage.show();
        }catch(Exception e){
            System.err.println("Error boton opcion 3. "+e.getMessage());
        }
    }
    
    
    private void limpiar(){
        this.codigo.setText("");
        this.date.setText("");
        this.porcentaje.setText("");

        this.nombres.setText("");
        this.apellidos.setText("");
        this.cedula.setText("");

        Vuelo v = new Vuelo(0);
        Silla silla;
        for (int i = 0; i < v.getSillas().length; i++) {
            for (int j = 0; j < v.getSillas()[i].length; j++) {
                silla = v.getSillas()[i][j];
                modificarCheckBox(silla.getSilla(), false, true);

            }
        }
    }
        
    ////////////////////////////////////////////////////////////////////////////
    private void modificarCheckBox(String silla,boolean selected,boolean disable){
        switch(silla){
        case "0-A": A0.setSelected(selected); A0.setDisable(disable); break; case "0-B": B0.setSelected(selected); B0.setDisable(disable); break; 
        case "0-C": C0.setSelected(selected); C0.setDisable(disable); break; case "0-D": D0.setSelected(selected); D0.setDisable(disable); break; case "0-E": E0.setSelected(selected); E0.setDisable(disable); break;
        
        case "1-A": A1.setSelected(selected); A1.setDisable(disable); break; case "1-B": B1.setSelected(selected); B1.setDisable(disable); break; 
        case "1-C": C1.setSelected(selected); C1.setDisable(disable); break; case "1-D": D1.setSelected(selected); D1.setDisable(disable); break; case "1-E": E1.setSelected(selected); E1.setDisable(disable); break;
        
        case "2-A": A2.setSelected(selected); A2.setDisable(disable); break; case "2-B": B2.setSelected(selected); B2.setDisable(disable); break; 
        case "2-C": C2.setSelected(selected); C2.setDisable(disable); break; case "2-D": D2.setSelected(selected); D2.setDisable(disable); break; case "2-E": E2.setSelected(selected); E2.setDisable(disable); break;
        
        case "3-A": A3.setSelected(selected); A3.setDisable(disable); break; case "3-B": B3.setSelected(selected); B3.setDisable(disable); break; 
        case "3-C": C3.setSelected(selected); C3.setDisable(disable); break; case "3-D": D3.setSelected(selected); D3.setDisable(disable); break; case "3-E": E3.setSelected(selected); E3.setDisable(disable); break;
        
        case "4-A": A4.setSelected(selected); A4.setDisable(disable); break; case "4-B": B4.setSelected(selected); B4.setDisable(disable); break; 
        case "4-C": C4.setSelected(selected); C4.setDisable(disable); break; case "4-D": D4.setSelected(selected); D4.setDisable(disable); break; case "4-E": E4.setSelected(selected); E4.setDisable(disable); break;
        
        case "5-A": A5.setSelected(selected); A5.setDisable(disable); break; case "5-B": B5.setSelected(selected); B5.setDisable(disable); break; 
        case "5-C": C5.setSelected(selected); C5.setDisable(disable); break; case "5-D": D5.setSelected(selected); D5.setDisable(disable); break; case "5-E": E5.setSelected(selected); E5.setDisable(disable); break;
        
        case "6-A": A6.setSelected(selected); A6.setDisable(disable); break; case "6-B": B6.setSelected(selected); B6.setDisable(disable); break; 
        case "6-C": C6.setSelected(selected); C6.setDisable(disable); break; case "6-D": D6.setSelected(selected); D6.setDisable(disable); break; case "6-E": E6.setSelected(selected); E6.setDisable(disable); break;
        
        case "7-A": A7.setSelected(selected); A7.setDisable(disable); break; case "7-B": B7.setSelected(selected); B7.setDisable(disable); break; 
        case "7-C": C7.setSelected(selected); C7.setDisable(disable); break; case "7-D": D7.setSelected(selected); D7.setDisable(disable); break; case "7-E": E7.setSelected(selected); E7.setDisable(disable); break;
        
        case "8-A": A8.setSelected(selected); A8.setDisable(disable); break; case "8-B": B8.setSelected(selected); B8.setDisable(disable); break; 
        case "8-C": C8.setSelected(selected); C8.setDisable(disable); break; case "8-D": D8.setSelected(selected); D8.setDisable(disable); break; case "8-E": E8.setSelected(selected); E8.setDisable(disable); break;
        
        case "9-A": A9.setSelected(selected); A9.setDisable(disable); break; case "9-B": B9.setSelected(selected); B9.setDisable(disable); break; 
        case "9-C": C9.setSelected(selected); C9.setDisable(disable); break; case "9-D": D9.setSelected(selected); D9.setDisable(disable); break; case "9-E": E9.setSelected(selected); E9.setDisable(disable); break;
        
        case "10-A": A10.setSelected(selected); A10.setDisable(disable); break; case "10-B": B10.setSelected(selected); B10.setDisable(disable); break; 
        case "10-C": C10.setSelected(selected); C10.setDisable(disable); break; case "10-D": D10.setSelected(selected); D10.setDisable(disable); break; case "10-E": E10.setSelected(selected); E10.setDisable(disable); break;
        
        case "11-A": A11.setSelected(selected); A11.setDisable(disable); break; case "11-B": B11.setSelected(selected); B11.setDisable(disable); break; 
        case "11-C": C11.setSelected(selected); C11.setDisable(disable); break; case "11-D": D11.setSelected(selected); D11.setDisable(disable); break; case "11-E": E11.setSelected(selected); E11.setDisable(disable); break;
        
        case "12-A": A12.setSelected(selected); A12.setDisable(disable); break; case "12-B": B12.setSelected(selected); B12.setDisable(disable); break; 
        case "12-C": C12.setSelected(selected); C12.setDisable(disable); break; case "12-D": D12.setSelected(selected); D12.setDisable(disable); break; case "12-E": E12.setSelected(selected); E12.setDisable(disable); break;
        
        case "13-A": A13.setSelected(selected); A13.setDisable(disable); break; case "13-B": B13.setSelected(selected); B13.setDisable(disable); break; 
        case "13-C": C13.setSelected(selected); C13.setDisable(disable); break; case "13-D": D13.setSelected(selected); D13.setDisable(disable); break; case "13-E": E13.setSelected(selected); E13.setDisable(disable); break;
        
        case "14-A": A14.setSelected(selected); A14.setDisable(disable); break; case "14-B": B14.setSelected(selected); B14.setDisable(disable); break; 
        case "14-C": C14.setSelected(selected); C14.setDisable(disable); break; case "14-D": D14.setSelected(selected); D14.setDisable(disable); break; case "14-E": E14.setSelected(selected); E14.setDisable(disable); break;
        }
    }
    
    
    @FXML
    private void handleCheckBoxA0Action(ActionEvent event){if(this.A0.isSelected()) this.sillas.add("0-A");else this.sillas.remove("0-A");}
    @FXML
    private void handleCheckBoxB0Action(ActionEvent event){if(this.B0.isSelected()) this.sillas.add("0-B");else this.sillas.remove("0-B");}
    @FXML
    private void handleCheckBoxC0Action(ActionEvent event){if(this.C0.isSelected()) this.sillas.add("0-C");else this.sillas.remove("0-C");}
    @FXML
    private void handleCheckBoxD0Action(ActionEvent event){if(this.D0.isSelected()) this.sillas.add("0-D");else this.sillas.remove("0-D");}
    @FXML
    private void handleCheckBoxE0Action(ActionEvent event){if(this.E0.isSelected()) this.sillas.add("0-E");else this.sillas.remove("0-E");}
    
    
    @FXML
    private void handleCheckBoxA1Action(ActionEvent event){if(this.A1.isSelected()) this.sillas.add("1-A");else this.sillas.remove("1-A");}
    @FXML
    private void handleCheckBoxB1Action(ActionEvent event){if(this.B1.isSelected()) this.sillas.add("1-B");else this.sillas.remove("1-B");}
    @FXML
    private void handleCheckBoxC1Action(ActionEvent event){if(this.C1.isSelected()) this.sillas.add("1-C");else this.sillas.remove("1-C");}
    @FXML
    private void handleCheckBoxD1Action(ActionEvent event){if(this.D1.isSelected()) this.sillas.add("1-D");else this.sillas.remove("1-D");}
    @FXML
    private void handleCheckBoxE1Action(ActionEvent event){if(this.E1.isSelected()) this.sillas.add("1-E");else this.sillas.remove("1-E");}
    
    
    @FXML
    private void handleCheckBoxA2Action(ActionEvent event){if(this.A2.isSelected()) this.sillas.add("2-A");else this.sillas.remove("2-A");}
    @FXML
    private void handleCheckBoxB2Action(ActionEvent event){if(this.B2.isSelected()) this.sillas.add("2-B");else this.sillas.remove("2-B");}
    @FXML
    private void handleCheckBoxC2Action(ActionEvent event){if(this.C2.isSelected()) this.sillas.add("2-C");else this.sillas.remove("2-C");}
    @FXML
    private void handleCheckBoxD2Action(ActionEvent event){if(this.D2.isSelected()) this.sillas.add("2-D");else this.sillas.remove("2-D");}
    @FXML
    private void handleCheckBoxE2Action(ActionEvent event){if(this.E2.isSelected()) this.sillas.add("2-E");else this.sillas.remove("2-E");}
    
    
    @FXML
    private void handleCheckBoxA3Action(ActionEvent event){if(this.A3.isSelected()) this.sillas.add("3-A");else this.sillas.remove("3-A");}
    @FXML
    private void handleCheckBoxB3Action(ActionEvent event){if(this.B3.isSelected()) this.sillas.add("3-B");else this.sillas.remove("3-B");}
    @FXML
    private void handleCheckBoxC3Action(ActionEvent event){if(this.C3.isSelected()) this.sillas.add("3-C");else this.sillas.remove("3-C");}
    @FXML
    private void handleCheckBoxD3Action(ActionEvent event){if(this.D3.isSelected()) this.sillas.add("3-D");else this.sillas.remove("3-D");}
    @FXML
    private void handleCheckBoxE3Action(ActionEvent event){if(this.E3.isSelected()) this.sillas.add("3-E");else this.sillas.remove("3-E");}
  
    
    @FXML
    private void handleCheckBoxA4Action(ActionEvent event){if(this.A4.isSelected()) this.sillas.add("4-A");else this.sillas.remove("4-A");}
    @FXML
    private void handleCheckBoxB4Action(ActionEvent event){if(this.B4.isSelected()) this.sillas.add("4-B");else this.sillas.remove("4-B");}
    @FXML
    private void handleCheckBoxC4Action(ActionEvent event){if(this.C4.isSelected()) this.sillas.add("4-C");else this.sillas.remove("4-C");}
    @FXML
    private void handleCheckBoxD4Action(ActionEvent event){if(this.D4.isSelected()) this.sillas.add("4-D");else this.sillas.remove("4-D");}
    @FXML
    private void handleCheckBoxE4Action(ActionEvent event){if(this.E4.isSelected()) this.sillas.add("4-E");else this.sillas.remove("4-E");}
    
    
    @FXML
    private void handleCheckBoxA5Action(ActionEvent event){if(this.A5.isSelected()) this.sillas.add("5-A");else this.sillas.remove("5-A");}
    @FXML
    private void handleCheckBoxB5Action(ActionEvent event){if(this.B5.isSelected()) this.sillas.add("5-B");else this.sillas.remove("5-B");}
    @FXML
    private void handleCheckBoxC5Action(ActionEvent event){if(this.C5.isSelected()) this.sillas.add("5-C");else this.sillas.remove("5-C");}
    @FXML
    private void handleCheckBoxD5Action(ActionEvent event){if(this.D5.isSelected()) this.sillas.add("5-D");else this.sillas.remove("5-D");}
    @FXML
    private void handleCheckBoxE5Action(ActionEvent event){if(this.E5.isSelected()) this.sillas.add("5-E");else this.sillas.remove("5-E");}
    
    
    @FXML
    private void handleCheckBoxA6Action(ActionEvent event){if(this.A6.isSelected()) this.sillas.add("6-A");else this.sillas.remove("6-A");}
    @FXML
    private void handleCheckBoxB6Action(ActionEvent event){if(this.B6.isSelected()) this.sillas.add("6-B");else this.sillas.remove("6-B");}
    @FXML
    private void handleCheckBoxC6Action(ActionEvent event){if(this.C6.isSelected()) this.sillas.add("6-C");else this.sillas.remove("6-C");}
    @FXML
    private void handleCheckBoxD6Action(ActionEvent event){if(this.D6.isSelected()) this.sillas.add("6-D");else this.sillas.remove("6-D");}
    @FXML
    private void handleCheckBoxE6Action(ActionEvent event){if(this.E6.isSelected()) this.sillas.add("6-E");else this.sillas.remove("6-E");}
    
    
    @FXML
    private void handleCheckBoxA7Action(ActionEvent event){if(this.A7.isSelected()) this.sillas.add("7-A");else this.sillas.remove("7-A");}
    @FXML
    private void handleCheckBoxB7Action(ActionEvent event){if(this.B7.isSelected()) this.sillas.add("7-B");else this.sillas.remove("7-B");}
    @FXML
    private void handleCheckBoxC7Action(ActionEvent event){if(this.C7.isSelected()) this.sillas.add("7-C");else this.sillas.remove("7-C");}
    @FXML
    private void handleCheckBoxD7Action(ActionEvent event){if(this.D7.isSelected()) this.sillas.add("7-D");else this.sillas.remove("7-D");}
    @FXML
    private void handleCheckBoxE7Action(ActionEvent event){if(this.E7.isSelected()) this.sillas.add("7-E");else this.sillas.remove("7-E");}
    
    
    @FXML
    private void handleCheckBoxA8Action(ActionEvent event){if(this.A8.isSelected()) this.sillas.add("8-A");else this.sillas.remove("8-A");}
    @FXML
    private void handleCheckBoxB8Action(ActionEvent event){if(this.B8.isSelected()) this.sillas.add("8-B");else this.sillas.remove("8-B");}
    @FXML
    private void handleCheckBoxC8Action(ActionEvent event){if(this.C8.isSelected()) this.sillas.add("8-C");else this.sillas.remove("8-C");}
    @FXML
    private void handleCheckBoxD8Action(ActionEvent event){if(this.D8.isSelected()) this.sillas.add("8-D");else this.sillas.remove("8-D");}
    @FXML
    private void handleCheckBoxE8Action(ActionEvent event){if(this.E8.isSelected()) this.sillas.add("8-E");else this.sillas.remove("8-E");}
    
    
    @FXML
    private void handleCheckBoxA9Action(ActionEvent event){if(this.A9.isSelected()) this.sillas.add("9-A");else this.sillas.remove("9-A");}
    @FXML
    private void handleCheckBoxB9Action(ActionEvent event){if(this.B9.isSelected()) this.sillas.add("9-B");else this.sillas.remove("9-B");}
    @FXML
    private void handleCheckBoxC9Action(ActionEvent event){if(this.C9.isSelected()) this.sillas.add("9-C");else this.sillas.remove("9-C");}
    @FXML
    private void handleCheckBoxD9Action(ActionEvent event){if(this.D9.isSelected()) this.sillas.add("9-D");else this.sillas.remove("9-D");}
    @FXML
    private void handleCheckBoxE9Action(ActionEvent event){if(this.E9.isSelected()) this.sillas.add("9-E");else this.sillas.remove("9-E");}
    
    
    @FXML
    private void handleCheckBoxA10Action(ActionEvent event){if(this.A10.isSelected()) this.sillas.add("10-A");else this.sillas.remove("10-A");}
    @FXML
    private void handleCheckBoxB10Action(ActionEvent event){if(this.B10.isSelected()) this.sillas.add("10-B");else this.sillas.remove("10-B");}
    @FXML
    private void handleCheckBoxC10Action(ActionEvent event){if(this.C10.isSelected()) this.sillas.add("10-C");else this.sillas.remove("10-C");}
    @FXML
    private void handleCheckBoxD10Action(ActionEvent event){if(this.D10.isSelected()) this.sillas.add("10-D");else this.sillas.remove("10-D");}
    @FXML
    private void handleCheckBoxE10Action(ActionEvent event){if(this.E10.isSelected()) this.sillas.add("10-E");else this.sillas.remove("10-E");}
    
    
    
    @FXML
    private void handleCheckBoxA11Action(ActionEvent event){if(this.A11.isSelected()) this.sillas.add("11-A");else this.sillas.remove("11-A");}
    @FXML
    private void handleCheckBoxB11Action(ActionEvent event){if(this.B11.isSelected()) this.sillas.add("11-B");else this.sillas.remove("11-B");}
    @FXML
    private void handleCheckBoxC11Action(ActionEvent event){if(this.C11.isSelected()) this.sillas.add("11-C");else this.sillas.remove("11-C");}
    @FXML
    private void handleCheckBoxD11Action(ActionEvent event){if(this.D11.isSelected()) this.sillas.add("11-D");else this.sillas.remove("11-D");}
    @FXML
    private void handleCheckBoxE11Action(ActionEvent event){if(this.E11.isSelected()) this.sillas.add("11-E");else this.sillas.remove("11-E");}
    
    
    
    @FXML
    private void handleCheckBoxA12Action(ActionEvent event){if(this.A12.isSelected()) this.sillas.add("12-A");else this.sillas.remove("12-A");}
    @FXML
    private void handleCheckBoxB12Action(ActionEvent event){if(this.B12.isSelected()) this.sillas.add("12-B");else this.sillas.remove("12-B");}
    @FXML
    private void handleCheckBoxC12Action(ActionEvent event){if(this.C12.isSelected()) this.sillas.add("12-C");else this.sillas.remove("12-C");}
    @FXML
    private void handleCheckBoxD12Action(ActionEvent event){if(this.D12.isSelected()) this.sillas.add("12-D");else this.sillas.remove("12-D");}
    @FXML
    private void handleCheckBoxE12Action(ActionEvent event){if(this.E12.isSelected()) this.sillas.add("12-E");else this.sillas.remove("12-E");}
    
    
    @FXML
    private void handleCheckBoxA13Action(ActionEvent event){if(this.A13.isSelected()) this.sillas.add("13-A");else this.sillas.remove("13-A");}
    @FXML
    private void handleCheckBoxB13Action(ActionEvent event){if(this.B13.isSelected()) this.sillas.add("13-B");else this.sillas.remove("13-B");}
    @FXML
    private void handleCheckBoxC13Action(ActionEvent event){if(this.C13.isSelected()) this.sillas.add("13-C");else this.sillas.remove("13-C");}
    @FXML
    private void handleCheckBoxD13Action(ActionEvent event){if(this.D13.isSelected()) this.sillas.add("13-D");else this.sillas.remove("13-D");}
    @FXML
    private void handleCheckBoxE13Action(ActionEvent event){if(this.E13.isSelected()) this.sillas.add("13-E");else this.sillas.remove("13-E");}
    
    
    @FXML
    private void handleCheckBoxA14Action(ActionEvent event){if(this.A14.isSelected()) this.sillas.add("14-A");else this.sillas.remove("14-A");}
    @FXML
    private void handleCheckBoxB14Action(ActionEvent event){if(this.B14.isSelected()) this.sillas.add("14-B");else this.sillas.remove("14-B");}
    @FXML
    private void handleCheckBoxC14Action(ActionEvent event){if(this.C14.isSelected()) this.sillas.add("14-C");else this.sillas.remove("14-C");}
    @FXML
    private void handleCheckBoxD14Action(ActionEvent event){if(this.D14.isSelected()) this.sillas.add("14-D");else this.sillas.remove("14-D");}
    @FXML
    private void handleCheckBoxE14Action(ActionEvent event){if(this.E14.isSelected()) this.sillas.add("14-E");else this.sillas.remove("14-E");}
    

    
    

    ////////////////////////////////////////////////////////////////////////////
    //GETTERS & SETTERS
    ////////////////////////////////////////////////////////////////////////////

    public static Aerolinea getAero() {
        return aero;
    }

}
