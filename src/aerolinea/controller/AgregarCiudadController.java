/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aerolinea.controller;

import aerolinea.Vuelo;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @Yoner Silva
 * @Leiddy Vacca
 */
public class AgregarCiudadController extends PrincipalController implements Initializable{

    @FXML
    private TextField cod_Y;
    @FXML
    private TextField cod_X;
    @FXML
    private Button agregarCiudad;
    @FXML
    private TextField ciudadAgregar;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
    
    @FXML
    private void handleaAgregarCiudadAction(ActionEvent event) {
        try{
            String ciudad = this.ciudadAgregar.getText();
            String cod_X = this.cod_X.getText();
            String cod_Y = this.cod_Y.getText();
            Vuelo v = new Vuelo();
            if(!(v.getOrigen().getCiudad().equals(ciudad.toUpperCase()))){
                if ((ciudad.equals("")) || (cod_X.equals("")) || (cod_Y.equals(""))) {
                    super.crearDialogo("¡Error!", "Las casillas no pueden estar vacías.");
                } else {
                    boolean addCiudad = super.getAero().addCiudad(ciudad, Double.parseDouble(cod_X), Double.parseDouble(cod_Y));
                    if (addCiudad) {
                        super.crearDialogo("Agregar Ciudad", "La ciudad " + ciudad + " fue añadida con exito.");
                        Stage stage = (Stage) agregarCiudad.getScene().getWindow();
                        stage.close();
                    } else {
                        super.crearDialogo("Agregar Ciudad", "La ciudad " + ciudad + " ya existe." + "\n" + "Intente de nuevo.");
                    }
                }
            }else{
                super.crearDialogo("¡Error!", "La ciudad de destino no puede ser igual a la de origen.");
            }
            
        }catch(Exception e){
            System.err.println("Error al agregar ciudad. "+e.getMessage());
        }
    }
    
}
