/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aerolinea.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @Yoner Silva
 * @Leiddy Vacca
 */
public class Opcion3Controller extends PrincipalController implements Initializable{
    
    @FXML
    private TextField nombre;
    @FXML
    private Button reservarVuelo;
    @FXML
    private TextField cedula;
    @FXML
    private TextField silla;
    @FXML
    private TextField codigo;

    

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
  
      
    @FXML
    private void handleaReservarVueloAction(ActionEvent event) {
        try{
            String codigo = this.codigo.getText();
            String nombre = this.nombre.getText();
            String cedula = this.cedula.getText();
            String silla = this.silla.getText();
            if((codigo.equals("")) || (nombre.equals("")) || (cedula.equals("")) || (silla.equals(""))){
                super.crearDialogo("¡Error!","Las casillas no pueden estar vacías.");
            }else{
                boolean reservarVuelo = super.getAero().reservarVuelo(Integer.parseInt(codigo), nombre, Integer.parseInt(cedula), silla.toUpperCase());
                if(reservarVuelo){
                    super.crearDialogo("Opcion 3","La reserva fue exitosa.");
                    Stage stage = (Stage) this.reservarVuelo.getScene().getWindow();
                    stage.close();
                }else{
                    super.crearDialogo("Opcion 3","La reserva no se pudo llevar a cabo, debido a que ya se encuentra ocupada la silla."+ '\n' 
                        + "Intente nuevamente. :(");
                }
            }
        }catch(Exception e){
            System.err.println("Error en reservar Vuelo. "+e.getMessage());
        }
    }

}
