package aerolinea;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @Yoner Silva
 * @Leiddy Vacca
 */
public class Reserva {
    private int codigo;
    private String nombre;
    private int cedula;
    
    //Lista de sillas reservadas por el usuario.
    private List<Silla> puestos;

    public Reserva() {
    }

    
    public Reserva(int codigo, String nombre, int cedula) {
        puestos = new ArrayList<>(5);
        this.codigo = codigo;
        this.cedula = cedula;
        this.nombre = nombre;
    }

    ////////////////////////////////////////////////////////////////////////////
    //GETTERS & SETTERS
    ////////////////////////////////////////////////////////////////////////////
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public List<Silla> getPuestos() {
        return puestos;
    }

    public int getCodigo() {
        return codigo;
    }
    
    
    
    
}
