/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aerolinea;

import java.util.Date;

/**
 *
 * @Yoner Silva
 * Leiddy Vacca
 */
public class Fecha implements Comparable<Fecha>{
    //FECHA
    private Date fecha;
    
    public Fecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return this.fecha.toString();
    }

    
    ////////////////////////////////////////////////////////////////////////////
    //GETTERS & SETTERS
    ////////////////////////////////////////////////////////////////////////////
    
    

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public int compareTo(Fecha other) {
        return this.getFecha().compareTo(other.getFecha());
    }
    
    
}
