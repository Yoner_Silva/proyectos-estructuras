package aerolinea;

/**
 *
 * @Yoner Silva
 * @Leiddy Vacca
 */
public class Silla {
    private String silla;
    private boolean estaOcupada;

    public Silla(String silla) {
        this.silla = silla;
        this.estaOcupada = false;
    }

    ////////////////////////////////////////////////////////////////////////////
    //GETTERS & SETTERS
    ////////////////////////////////////////////////////////////////////////////
    
    public String getSilla() {
        return silla;
    }

    
    public boolean isEstaOcupada() {
        return estaOcupada;
    }

    public void setEstaOcupada(boolean estaOcupada) {
        this.estaOcupada = estaOcupada;
    }
    
    
}
