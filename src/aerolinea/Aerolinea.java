package aerolinea;

import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @Yoner Silva
 * @Leiddy Vacca
 */
public class Aerolinea {
    //Lista de vuelos.
    private List<Vuelo> vuelos;
    
    //Lista de ciudades.
    private List<Ciudad> ciudades;
    
    public Aerolinea() {
        vuelos = new LinkedList<>();
        ciudades = new LinkedList<>();
    }
    
    /**
     * @param codigo
     * @param date
     * @param destino
     * Retorna verdadero o falso si pudo o no agregar un vuelo a la lista
     * de vuelos.
     */
    public boolean addVuelo(int codigo, Date date, Ciudad destino){
        Fecha fecha = new Fecha(date);
        if(validarAddVuelo(codigo, date, destino)){
            Vuelo v = new Vuelo(codigo, fecha, destino);
            vuelos.add(v);
            return true;
        }
        return false;
    }
    
    /**
     * @param codigo
     * @param date
     * @param destino
     * Retorna verdadero o falso si encuentra o no un vuelo con los mismo atributos.
     *   1) Si se encontrara un vuelo con el mismo codigo, sería falso.
     *   2) Si se encontrase un vuelo con un codigo diferente a los demas pero con la misma ciudad 
     *      destino, la misma fecha y hora. Entonces, sería falso.
     *   3) Si lo anterior no sucede, entonces sería verdadero. Se puede agregar el vuelo.
     */
    private boolean validarAddVuelo(int codigo,Date date, Ciudad destino){
        Iterator<Vuelo> it = vuelos.iterator();
        while(it.hasNext()){
            Vuelo v = it.next();
            if(v.getCodigo() != codigo){
                Fecha fecha = new Fecha(date);
                if((v.getDate().compareTo(fecha) == 0)&&(v.getDestino().compareTo(destino)== 0))
                    return false;
            }else{
                return false;
            }
        }
        return true;
    }
    
    /**
     * @param codigo
     * Busca un vuelo especifico en la lista de vuelos. 
     * Opcion 2
     */
    public Vuelo buscarVuelo(int codigo){
        Iterator<Vuelo> it = vuelos.iterator();
        while(it.hasNext()){
            Vuelo v = it.next();
            if(v.getCodigo()==codigo)
                return v;
        }
        return null;
    }
    
    /**
     * @param codigo
     * retorna verdadero o falso si pudo remover un vuelo de la lista
     * de vuelos.
     */
    public boolean removeVuelo(int codigo){
        Vuelo v = buscarVuelo(codigo);
        if(v != null){
            this.vuelos.remove(v);
            return true;
        }
        return false;
    }
    
    private void eliminarVuelos(Ciudad c){
        for (int i = 0; i < this.vuelos.size(); i++) {
            if(this.vuelos.get(i).getDestino().compareTo(c)==0)
                this.vuelos.remove(i);
        }
    }
    
    /**
     * @param ciudad
     * @param cod_X
     * @param cod_Y
     * Retorna verdadero o falso si pudo agregar una nueva ciudad a la lista
     * de ciudades.
     */
    public boolean addCiudad(String ciudad, double cod_X, double cod_Y){
        ciudad = ciudad.toUpperCase();
        if(buscarCiudad(ciudad)==null){
            Ciudad c = new Ciudad(ciudad, cod_X, cod_Y);
            this.ciudades.add(c);
            return true;
        }
        return false;
    }
    
    /**
     * @param ciudad
     * Retorna falso o verdadero si encuentra o no la ciudad en la lista de 
     * ciudades.
     */
    public Ciudad buscarCiudad(String ciudad){
        ciudad = ciudad.toUpperCase();
        Iterator<Ciudad> it = this.ciudades.iterator();
        while (it.hasNext()) {
            Ciudad c = it.next();
            if(c.getCiudad().equals(ciudad))
                return c;
        }
        return null;
    }
    
    /**
     * @param ciudad
     * Retorna falso o verdadero si puede remover la ciudad en la lista de 
     * ciudades.
     */
    public boolean removeCiudad(String ciudad){
        Ciudad c = buscarCiudad(ciudad);
        if(c !=null){
            this.ciudades.remove(c);
            eliminarVuelos(c);
            return true;
        }
        return false;
    }
    
    /**
     * 
     * @param vuelo
     * @param nombre
     * @param cedula
     * @param silla
     * Retonra verdadero y falso si se puede añadir una reserva.
     */
    public boolean reservarVuelo(int codigo,String nombre, int cedula, String silla){
        Vuelo vuelo = buscarVuelo(codigo);
        return vuelo.reservarVuelo(nombre, cedula, silla);
    }
    
    public boolean cancelarReserva(int codigo, String nombre, int cedula){
            Vuelo v = buscarVuelo(codigo);
            if(v != null){
                for (Reserva reserva: v.getReservas()) {
                    if((reserva.getCodigo() == codigo) && (reserva.getNombre().equals(nombre)) && (reserva.getCedula()==cedula)){
                        v.cancelarReserva(reserva.getPuestos());
                        v.getReservas().remove(reserva);
                        return true;
                    }
                }
            }
        return false;
    }
    
    /**
     * @param destino
     * Retorna una lista con todos los  vuelos que tengan como ciudad destino la 
     * dada en el parametro, ordenada por fecha y hora respectivamente.
     */
    public List<Vuelo> opcion1(Ciudad destino){
        List<Vuelo> aux  = new LinkedList<>();
        Iterator<Vuelo> it = this.vuelos.iterator();
        while (it.hasNext()) {
            Vuelo v = it.next();
            if(v.getDestino().compareTo(destino)==0)
                aux.add(v);
        }
        if(aux.size()>0){
            Collections.sort(aux);
            return aux;
        }
        return aux;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    //GETTERS & SETTERS
    ////////////////////////////////////////////////////////////////////////////

    public List<Vuelo> getVuelos() {
        return vuelos;
    }

    public List<Ciudad> getCiudades() {
        return ciudades;
    }
    
    
}
